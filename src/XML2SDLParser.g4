parser grammar XML2SDLParser;

options { tokenVocab=XML2SDLLexer; }

//=====================================
//PARSER
//=====================================
//XML Doc
document : xmlElem* EOF;

//XML Element
xmlElem : airportStart
          optionalElems*
          (taxiwaypointElem+
          taxiwayparkingElem+
          taxiNameElem+
          taxiwayPathElem+)?
          optionalElems*
          airportEnd;

optionalElems : serviceElem
                | towerElem
                | runwayElem
                | comElem
                | helipadElem;

//=====================================
//Airport
//=====================================
airportAttrReal : LAT
                | LON
                | MAGVAR
                | TRAFFIC_SCALAR
                | AIRPORT_TEST_RADIUS;

airportAttrInt : MAGVAR;

airportAttrStr : REGION
               | COUNTRY
               | STATE
               | CITY
               | NAME
               | ALT
               | IDENT
               | AIRPORT_TEST_RADIUS;

airportExpReal: airportAttrReal ATTR_EQ ATTR_VALUE_REAL;
airportExpInt: airportAttrInt ATTR_EQ ATTR_VALUE_INT;
airportExpStr: airportAttrStr ATTR_EQ ATTR_VALUE_STR;

airportStart : AIRPORT_TAG_START
             (airportExpReal | airportExpInt | airportExpStr)*
             TAG_CLOSE; 

airportEnd : TAG_END_OPEN AIRPORT_TAG_CLOSE;

//=====================================
//TaxiwayPoint
//=====================================
taxiwayPointAttStr: TYPE | ORIENTATION;

taxiwayPointAttReal: BIASX |
                     BIASZ |
                     LON |
                     LAT;

taxiwayPointAttInt: INDEX; 

taxiwayPointExpReal: taxiwayPointAttReal ATTR_EQ ATTR_VALUE_REAL; 
taxiwayPointExpInt: taxiwayPointAttInt ATTR_EQ ATTR_VALUE_INT;
taxiwayPointExpStr: taxiwayPointAttStr ATTR_EQ ATTR_VALUE_STR; 

taxiwaypointElem : TAG_START_OPEN TAXIWAYPOINT
                   (taxiwayPointExpReal | taxiwayPointExpInt | taxiwayPointExpStr)* 
                   TAG_EMPTY_CLOSE;

//=====================================
//Services
//=====================================
serviceStart : TAG_START_OPEN SERVICES TAG_CLOSE;

serviceEnd : TAG_END_OPEN SERVICES TAG_CLOSE;

serviceElem : serviceStart fuelElem* serviceEnd;

//=====================================
//Fuel
//=====================================
fuelAttrStr : TYPE | AVAILABILITY;

fuelExpStr: fuelAttrStr ATTR_EQ ATTR_VALUE_STR;
fuelExpInt: TYPE ATTR_EQ ATTR_VALUE_INT;

fuelElem : TAG_START_OPEN FUEL 
         (fuelExpStr | fuelExpInt)*
         TAG_EMPTY_CLOSE;

//=====================================
//Tower
//=====================================
towerAttrReal : LAT | LON;

towerAttrStr : ALT;

towerExpReal: towerAttrReal ATTR_EQ ATTR_VALUE_REAL;
towerExpStr: towerAttrStr ATTR_EQ ATTR_VALUE_STR;

towerStart : TAG_START_OPEN TOWER
          (towerExpReal | towerExpStr)*;

towerEnd : TAG_EMPTY_CLOSE | (TAG_CLOSE TAG_END_OPEN TOWER TAG_CLOSE);

towerElem : towerStart towerEnd;

//=====================================
//TaxiwayParking
//=====================================
taxiwayparkingAttrInt: INDEX
                     | NUMBER
                     | HEADING;
                                 
taxiwayparkingAttrReal: LAT
                      | LON
                      | BIASX
                      | BIASZ
                      | HEADING
                      | RADIUS
                      | TEEOFFSET1
                      | TEEOFFSET2
                      | TEEOFFSET3
                      | TEEOFFSET4;
                          
taxiwayparkingAttrStr: TYPE
                     | NAME
                     | AIRLINECODES
                     | PUSHBACK
                     | RADIUS;

taxiwayparkingExpInt: taxiwayparkingAttrInt ATTR_EQ ATTR_VALUE_INT;
taxiwayparkingExpReal: taxiwayparkingAttrReal ATTR_EQ ATTR_VALUE_REAL;
taxiwayparkingExpStr: taxiwayparkingAttrStr ATTR_EQ ATTR_VALUE_STR;

taxiwayparkingElem : TAG_START_OPEN TAXIWAYPARKING
                      (taxiwayparkingExpInt | taxiwayparkingExpReal | taxiwayparkingExpStr)*
                   TAG_EMPTY_CLOSE;

//=====================================
//ROUTE
//=====================================
routeAttrStr : ROUTETYPE
             | NAME;

routeExpStr: routeAttrStr ATTR_EQ ATTR_VALUE_STR;

routeStart:  TAG_START_OPEN ROUTE
             (routeExpStr)*
             TAG_CLOSE; 

routeEnd : TAG_END_OPEN ROUTE TAG_CLOSE;

routeElem : routeStart
         (previouselement nextelement)?
          routeEnd;

//=====================================
//PREVIOUS
//=====================================
previousAttrStr : WAYPOINTTYPE
                | WAYPOINTREGION
                | WAYPOINTIDENT;
                   
previousExpStr: previousAttrStr ATTR_EQ ATTR_VALUE_STR;
previousExpReal: ALTITUDEMINIMUN ATTR_EQ (ATTR_VALUE_REAL | ATTR_VALUE_INT);


previouselement : TAG_START_OPEN PREVIOUS
                  (previousExpStr | previousExpReal)*
                  TAG_EMPTY_CLOSE;

//=====================================
//NEXT
//=====================================
nextAttrStr : WAYPOINTTYPE
               | WAYPOINTREGION
               | WAYPOINTIDENT;
                   
nextAttrReal : ALTITUDEMINIMUN;

nextExpStr: nextAttrStr ATTR_EQ ATTR_VALUE_STR;
nextExpReal: ALTITUDEMINIMUN ATTR_EQ (ATTR_VALUE_REAL | ATTR_VALUE_INT);

nextelement : TAG_START_OPEN NEXT
                  (nextExpStr | nextExpReal)*
                  TAG_EMPTY_CLOSE;

//=====================================
//TaxiwayPath
//=====================================
taxiwayPathAttrInt: START | 
                    END |
                    NAME | 
                    WEIGHT_LIMIT |
                    NUMBER;

taxiwayPathAttrReal: WIDTH |
                     WEIGHT_LIMIT;

taxiwayPathAttrString: TYPE |
                       SURFACE |
                       DRAW_SURFACE |
                       DRAW_DETAIL |
                       CENTER_LINE |
                       CENTER_LINE_LIGHTED |
                       LEFT_EDGE |
                       LEFT_EDGE_LIGHTED |
                       RIGHT_EDGE |
                       RIGHT_EDGE_LIGHTED |
                       NUMBER |
                       DESIGNATOR |
                       WIDTH;

taxiwayPathExpInt: taxiwayPathAttrInt ATTR_EQ ATTR_VALUE_INT;
taxiwayPathExpReal: taxiwayPathAttrReal ATTR_EQ ATTR_VALUE_REAL;
taxiwayPathExpStr: taxiwayPathAttrString ATTR_EQ ATTR_VALUE_STR;

taxiwayPathElem : TAG_START_OPEN TAXIWAYPATH 
                  (taxiwayPathExpInt | taxiwayPathExpReal | taxiwayPathExpStr)*
                   TAG_EMPTY_CLOSE;

//=====================================
//RunwayAlias
//=====================================
/*
runwayAliasAttrString: DESIGNATOR |
                       NUMBER;

runwayAliasElem: TAG_START_OPEN RUNWAY_ALIAS
                 ((runwayAliasAttrString ATTR_EQ ATTR_VALUE_STR))*
                TAG_EMPTY_CLOSE;
*/
//=====================================
//Com
//=====================================
comAttrStr: TYPE | NAME;

comExpReal: FREQUENCY ATTR_EQ ATTR_VALUE_REAL;
comExpStr: comAttrStr ATTR_EQ ATTR_VALUE_STR;

comElem: TAG_START_OPEN COM 
         (comExpReal | comExpStr)+
         TAG_EMPTY_CLOSE;

//=====================================
//TaxiName
//=====================================
taxiNameExpInt: INDEX ATTR_EQ ATTR_VALUE_INT; 
taxiNameExpStr: NAME ATTR_EQ ATTR_VALUE_STR;

taxiNameElem: TAG_START_OPEN TAXINAME 
              (taxiNameExpInt | taxiNameExpStr)+
              TAG_EMPTY_CLOSE;

//=====================================
//Runway
//=====================================
runwayAttString: 
                 ALT |
                 SURFACE |
                 LENGTH |
                 WIDTH |
                 NUMBER |
                 DESIGNATOR |
                 PRIMARY_DESIGNATOR |
                 SECONDARY_DESIGNATOR |
                 PATTERN_ALTITUDE |                
                 PRIMARY_TAKE_OFF |
                 PRIMARY_LANDING |
                 PRIMARY_PATTERN |
                 SECONDARY_TAKE_OFF |
                 SECONDARY_LANDING |
                 SECONDARY_PATTERN |
                 PRIMARY_MARKING_BIAS |
                 SECONDARY_MARKING_BIAS;

runwayAttReal: LAT |
               LON |
               HEADING |
               ALT |
               NUMBER |
               DESIGNATOR |
               PRIMARY_DESIGNATOR |
               SECONDARY_DESIGNATOR |
               PATTERN_ALTITUDE;

runwayAttInt: NUMBER
            | HEADING;

runwayNestedElem: markingsElem
                | lightsElem
                | offsetThresholdElem
                | blastPadElem
                | overrunElem
                | approachLightsElem
                | vasiElem
                | ilsElem
                | runwayStartElem;

runwayExpString: runwayAttString ATTR_EQ ATTR_VALUE_STR;
runwayExpReal: runwayAttReal ATTR_EQ ATTR_VALUE_REAL;
runwayExpInt: runwayAttInt ATTR_EQ ATTR_VALUE_INT;

runwayElemStart: TAG_START_OPEN RUNWAY 
            (runwayExpString | runwayExpReal | runwayExpInt)*
            TAG_CLOSE;

runwayElemClose: TAG_END_OPEN RUNWAY TAG_CLOSE;

runwayElem: runwayElemStart runwayNestedElem* runwayElemClose;


//=====================================
//Markings
//=====================================
markingsAttsBool: 
                  ALTERNATETHRESHOLD |
                  ALTERNATETOUCHDOWN |
                  ALTERNATEFIXEDDISTANCE |
                  ALTERNATEPRECISION |
                  LEADINGZEROSIDENT |
                  NOTHRESHOLDENDARROWS |
                  EDGES |
                  THRESHOLD |
                  FIXED |
                  TOUCHDOWN |
                  DASHES |
                  IDENT |
                  PRECISION |
                  EDGEPAVEMENT |
                  SINGLEEND |
                  PRIMARYCLOSE |
                  SECONDARYCLOSED |
                  PRIMARYSTOL |
                  SECONDARYSTOL |
                  FIXED_DISTANCE;

markingsExpBool: markingsAttsBool ATTR_EQ ATTR_VALUE_STR; 

markingsElem : TAG_START_OPEN MARKINGS
               (markingsExpBool)+
               TAG_EMPTY_CLOSE;

//=====================================
//Lights
//=====================================
lightsAttStr: CENTER 
            | EDGE
            | CENTERRED;

lightsExpStr: lightsAttStr ATTR_EQ ATTR_VALUE_STR;

lightsElem : TAG_START_OPEN LIGHTS
               (lightsExpStr)+
               TAG_EMPTY_CLOSE;

//=====================================
//OffsetThreshold
//=====================================
offsetThresholdAttStr: END |
                     WIDTH |
                     LENGTH |
                     SURFACE;

offsetThresholdExpStr: offsetThresholdAttStr ATTR_EQ ATTR_VALUE_STR;

offsetThresholdElem : TAG_START_OPEN OFFSETTHRESHOLD
                      (offsetThresholdExpStr)*
                      TAG_EMPTY_CLOSE;

//=====================================
//BlastPad
//=====================================
blastPadAttStr: END |
               LENGTH |
               WIDTH |
               SURFACE;

blastPadExpStr: blastPadAttStr ATTR_EQ ATTR_VALUE_STR;

blastPadElem: TAG_START_OPEN BLASTPAD
              (blastPadExpStr)+
              TAG_EMPTY_CLOSE;

//=====================================
//Overrun
//=====================================
overrunAttStr: END |
               LENGTH |
               WIDTH |
               SURFACE;

overrunExpStr: overrunAttStr ATTR_EQ ATTR_VALUE_STR;

overrunElem: TAG_START_OPEN OVERRUN
              (overrunExpStr)+
              TAG_EMPTY_CLOSE;

//=====================================
//ApproachLights
//=====================================
approachLightsAttStr: END 
                    | SYSTEM
                    | REIL 
                    | TOUCHDOWN 
                    | ENDLIGHTS;

approachLightsExpInt: STROBES ATTR_EQ ATTR_VALUE_INT;
approachLightsExpStr: approachLightsAttStr ATTR_EQ ATTR_VALUE_STR; 

approachLightsElem: TAG_START_OPEN APPROACHLIGHTS
                    (approachLightsExpInt | approachLightsExpStr)+
                    TAG_EMPTY_CLOSE;

//=====================================
//Vasi
//=====================================
vasiAttStr: END |
          TYPE |
          SIDE |
          BIASX |
          BIASZ |
          SPACING;

vasiExpStr: vasiAttStr ATTR_EQ ATTR_VALUE_STR;
vasiExpInt: PITCH ATTR_EQ (ATTR_VALUE_REAL | ATTR_VALUE_INT);

vasiElem: TAG_START_OPEN VASI
          (vasiExpStr | vasiExpInt)+
          TAG_EMPTY_CLOSE;

//=====================================
//ILS
//=====================================
ilsAttStr: NAME
         | END
         | IDENT
         | BACKCOURSE
         | ALT
         | RANGE;

ilsAttReal: LAT |
           LON |
           HEADING |
           FREQUENCY |
           MAGVAR |
           WIDTH;

ilsAttInt: MAGVAR
         | HEADING
         | WIDTH;

ilsExpStr: ilsAttStr ATTR_EQ ATTR_VALUE_STR;
ilsExpReal: ilsAttReal ATTR_EQ ATTR_VALUE_REAL;
ilsExpInt: ilsAttInt ATTR_EQ ATTR_VALUE_INT; 

ilsElemStart:
        TAG_START_OPEN ILS
        (ilsExpStr | ilsExpReal | ilsExpInt)+
        TAG_CLOSE;

ilsElemClose:
             TAG_END_OPEN ILS TAG_CLOSE;

ilsElem: ilsElemStart
         glideSlopeElem?
         dmeElem?
         visualModelElem?
         ilsElemClose;

//=====================================
//GlideSlope
//=====================================
glideSlopeAttReal: LAT
                 | LON
                 | ALT
                 | PITCH;

glideSlopeAttStr: LAT
                 | LON
                 | RANGE
                 | ALT;

glideSlopeAttInt: RANGE
                | PITCH;

glideSlopeExpReal: glideSlopeAttReal ATTR_EQ ATTR_VALUE_REAL;
glideSlopeExpInt: glideSlopeAttInt ATTR_EQ ATTR_VALUE_INT;
glideSlopeExpStr: glideSlopeAttStr ATTR_EQ ATTR_VALUE_STR;

glideSlopeElem:   TAG_START_OPEN GLIDESLOPE
                  (glideSlopeExpReal | glideSlopeExpInt | glideSlopeExpStr)+
                  TAG_EMPTY_CLOSE;

//=====================================
//DME
//=====================================
dmeAttReal: LAT
                 | LON
                 | ALT
                 ;
dmeAttStr: LAT
                 | LON
                 | ALT
                 | RANGE
                 ;

dmeExpReal: dmeAttReal ATTR_EQ ATTR_VALUE_REAL;
dmeExpInt: RANGE ATTR_EQ ATTR_VALUE_INT;
dmeExpStr: dmeAttStr ATTR_EQ ATTR_VALUE_STR;

dmeElem:   TAG_START_OPEN DME
                  (dmeExpReal | dmeExpInt | dmeExpStr)+
                  TAG_EMPTY_CLOSE;

//=====================================
//VisualModel
//=====================================
visualModelAttStr: IMAGECOMPLEXITY
                 | NAME;

visualModelExpReal: HEADING ATTR_EQ ATTR_VALUE_REAL;

visualModelExpInt: HEADING ATTR_EQ ATTR_VALUE_INT;

visualModelExpStr: visualModelAttStr ATTR_EQ ATTR_VALUE_STR;

visualModelElemStart: TAG_START_OPEN VISUALMODEL
                  (visualModelExpReal | visualModelExpStr | visualModelExpInt) +
                  TAG_CLOSE;

visualModelElemClose:
             TAG_END_OPEN VISUALMODEL TAG_CLOSE;

visualModelElem: visualModelElemStart biasXYZElem? visualModelElemClose;

//=====================================
//BiasXYZ
//=====================================
biasXYZAttReal: BIASX
              | BIASY
              | BIASZ;

biasXYZAttInt: BIASX
             | BIASY
             | BIASZ;
                
biasXYZExpReal: biasXYZAttReal ATTR_EQ ATTR_VALUE_REAL;
biasXYZExpInt: biasXYZAttInt ATTR_EQ ATTR_VALUE_INT;

biasXYZElem: TAG_START_OPEN BiasXYZ
                  (biasXYZExpReal | biasXYZExpInt)+
                  TAG_EMPTY_CLOSE;

//=====================================
//RunwayStart
//=====================================
runwayStartAttReal: LAT
                  | LON
                  | ALT
                  | HEADING;

runwayStartAttStr: TYPE
                 | END;

runwayStartExpInt: HEADING ATTR_EQ ATTR_VALUE_INT;
runwayStartExpReal: runwayStartAttReal ATTR_EQ ATTR_VALUE_REAL;
runwayStartExpStr: runwayStartAttStr ATTR_EQ ATTR_VALUE_STR;

runwayStartElem: TAG_START_OPEN RUNWAYSTART
                 (runwayStartExpReal | runwayStartExpStr)+
                 TAG_EMPTY_CLOSE;

//=====================================
//Helipad
//=====================================
helipadAttStr: LAT
             | LON
             | ALT
             | SURFACE
             | LENGTH
             | WIDTH
             | TYPE
             | CLOSED
             | TRANSPARENT;

helipadAttReal: HEADING
              | LENGTH
              | WIDTH
              | LAT
              | LON;

helipadAttInt: RED
             | GREEN
             | BLUE
             | HEADING;

helipadExpStr: helipadAttStr ATTR_EQ ATTR_VALUE_STR;
helipadExpInt: helipadAttInt ATTR_EQ ATTR_VALUE_INT;
helipadExpReal: helipadAttReal ATTR_EQ ATTR_VALUE_REAL;

helipadElem: TAG_START_OPEN HELIPAD
             (helipadExpStr | helipadExpInt | helipadExpReal) *
             TAG_EMPTY_CLOSE;

//=====================================
//Ndb
//=====================================
/*
ndbAttStr: LAT
         | LON
         | TYPE
         | RANGE
         | REGION
         | IDENT
         | NAME;

ndbAttReal: LAT
         | LON
         | ALT
         | FREQUENCY
         | MAGVAR;

ndbAttInt: LAT
         | LON
         | ALT
         | RANGE;

ndbElemStart: TAG_START_OPEN NDB
                  ( ndbAttReal ATTR_EQ ATTR_VALUE_REAL
                  | ndbAttStr ATTR_EQ ATTR_VALUE_STR
                  | ndbAttInt ATTR_EQ ATTR_VALUE_INT)+
                  TAG_CLOSE;

ndbElemClose: TAG_END_OPEN NDB TAG_CLOSE;

ndbElem: ndbElemStart visualModelElem* ndbElemClose ;
*/
//=====================================
//Start
//=====================================
/*
startAttrInt: NUMBER;

startAttrReal: LAT 
             | LON
             | HEADING;

startAttrString: TYPE
               | NUMBER 
               | DESIGNATOR
               | ALT;
           
startElem: TAG_START_OPEN START_TAG
           ((startAttrInt ATTR_EQ ATTR_VALUE_INT) |
           (startAttrReal ATTR_EQ ATTR_VALUE_REAL) |
           (startAttrString ATTR_EQ ATTR_VALUE_STR))*
           TAG_EMPTY_CLOSE;
*/
//=====================================
//TaxiwaySign
//=====================================
/*
taxiwaySignAttrReal: LAT
                   | LON
                   | HEADING;


taxiwaySignAttrString: LABEL
                     | JUSTIFICATION 
                     | SIZE;
                 
taxiwaySignElem: TAG_START_OPEN TAXIWAYSIGN
                 ((taxiwaySignAttrReal ATTR_EQ ATTR_VALUE_REAL) |
                 (taxiwaySignAttrString ATTR_EQ ATTR_VALUE_STR))*
                 TAG_EMPTY_CLOSE;
*/
//=====================================
//Vertex
//=====================================
vertexAttrReal: BIASX 
              | BIASZ
              | LAT
              | LON;

vertexExpReal: vertexAttrReal ATTR_EQ ATTR_VALUE_REAL;

vertexElem: TAG_START_OPEN VERTEX
            (vertexExpReal)*
            TAG_EMPTY_CLOSE;

//=====================================
//Geopol
//=====================================
geopolAttrString: TYPE;

geopolExpString: geopolAttrString ATTR_EQ ATTR_VALUE_STR;

geopolElemStart: TAG_START_OPEN GEOPOL
                 geopolExpString
                 TAG_CLOSE;

geopolElemClose: TAG_END_OPEN GEOPOL TAG_CLOSE;

geopolElem: geopolElemStart vertexElem* geopolElemClose;

//=====================================
//Marker
//=====================================
markerAttrReal: LAT 
              | LON 
              | ALT 
              | HEADING;

markerAttrString: TYPE 
                | REGION
                | IDENT;

markerExpInt: HEADING ATTR_EQ ATTR_VALUE_INT;
markerExpReal: markerAttrReal ATTR_EQ ATTR_VALUE_REAL;
markerExpString: markerAttrString ATTR_EQ ATTR_VALUE_STR;

markerElem: TAG_START_OPEN MARKER 
            (markerExpReal | markerExpString)*
            TAG_EMPTY_CLOSE;

//=====================================
//Waypoint
//=====================================
/*
waypointAttrReal: LAT 
                | LON
                | MAGVAR;

waypointAttrInt: MAGVAR;

waypointAttrString: WAYPOINTTYPE
                  | WAYPOINTREGION
                  | WAYPOINTIDENT;

waypointElemStart: TAG_START_OPEN WAYPOINT 
                   ((waypointAttrReal ATTR_EQ ATTR_VALUE_REAL) 
                   |(waypointAttrString ATTR_EQ ATTR_VALUE_STR)
                   |(waypointAttrInt ATTR_EQ ATTR_VALUE_INT))*
                   TAG_CLOSE;

waypointElemClose: TAG_END_OPEN WAYPOINT TAG_CLOSE;

waypointElem: waypointElemStart routeElem* waypointElemClose;
*/
//=====================================
//APPROACH
//=====================================
/*
approachAttrStr : TYPE
                | FIXTYPE
                | FIXREGION
                | FIXIDENT
                | RUNWAY_ATTR
                | DESIGNATOR
                | SUFFIX
                | GPSOVERLAY
                | MISSALTITUDE
                | ALTITUDE;

approachAttrReal : ALTITUDE 
                 | HEADING
                 | MISSALTITUDE;
 
approachAttrInt : RUNWAY_ATTR
                | SUFFIX;

approachStart:  TAG_START_OPEN APPROACH
            ((approachAttrStr ATTR_EQ ATTR_VALUE_STR)  
             | (approachAttrInt ATTR_EQ ATTR_VALUE_INT) 
             | (approachAttrReal ATTR_EQ ( ATTR_VALUE_REAL | ATTR_VALUE_INT )))*
             TAG_CLOSE; 

approachEnd : TAG_END_OPEN APPROACH TAG_CLOSE;

approachElem : approachStart approachlegsElem* missedapproachlegsElem* transitionElem* approachEnd;
    */
//=====================================
//approachlegs
//=====================================
/*
approachlegsElem : TAG_START_OPEN APPROACHLEGS TAG_CLOSE
                  (legelement)*
                  TAG_END_OPEN APPROACHLEGS TAG_CLOSE;
*/
//=====================================
//Leg
//=====================================
/*  
legAttrStr :    TYPE
                | FIXTYPE
                | FIXREGION
                | FIXIDENT
                | FLYOVER
                | TURNDIRECTION
                | RECOMMENDEDTYPE
                | RECOMMENDEREGION
                | RECOMMENDEIDENT
                | RHO
                | DISTANCE
                | ALTITUDE1
                | ALTITUDE2
                | ALTITUDEDESCRIPTOR;

legAttrReal :   THETA
                | RHO
                | TRUECOURSE
                | MAGNETICCOURSE 
                | DISTANCE 
                | TIME 
                | ALTITUDE1 
                | ALTITUDE2;

legelement : TAG_START_OPEN LEG
             ((legAttrStr ATTR_EQ ATTR_VALUE_STR)  | (legAttrReal ATTR_EQ ( ATTR_VALUE_REAL | ATTR_VALUE_INT )))*
                   TAG_EMPTY_CLOSE;
*/
//=====================================
//MissedApproachLegs
//=====================================
/*
missedapproachlegsElem : TAG_START_OPEN MISSEDAPPROACHLEAGS TAG_CLOSE
                  (legelement)*
                  TAG_END_OPEN MISSEDAPPROACHLEAGS TAG_CLOSE;
*/
//=====================================
//Transition
//=====================================
/*
transitionAttrStr : TRANSITIONTYPE
                    | FIXTYPE
                    | FIXREGION
                    | FIXIDENT
                    | ALTITUDE;

transitionAttrReal : ALTITUDE;
 
transitionAttrInt :  ;

 transitionElem :transitionStart
            dmearc*
        transitionlegs+
          transitionEnd;

transitionStart:  TAG_START_OPEN TRANSITION
            ( (transitionAttrStr ATTR_EQ ATTR_VALUE_STR)  | (transitionAttrInt ATTR_EQ ATTR_VALUE_INT) | (transitionAttrReal ATTR_EQ ( ATTR_VALUE_REAL | ATTR_VALUE_INT )))*
             TAG_CLOSE; 

transitionEnd : TAG_END_OPEN TRANSITION TAG_CLOSE;
*/
//=====================================
//DmeArc
//=====================================
/*
dmearcAttrStr : DISTANCE
                | REGION
                | DMELDENT;
          
dmearcAttrReal : ;
           
dmearcAttrInt : RADIAL 
                | DISTANCE
                | REGION;

dmearc : TAG_START_OPEN DMEARC 
         ((dmearcAttrStr ATTR_EQ ATTR_VALUE_STR)  
          | (dmearcAttrInt ATTR_EQ ATTR_VALUE_INT) 
          | (dmearcAttrReal ATTR_EQ ( ATTR_VALUE_REAL | ATTR_VALUE_INT )))*
         TAG_EMPTY_CLOSE;
*/
//=====================================
//TRANSITIONLEGS
//=====================================
/*
transitionlegs:    TAG_START_OPEN TRANSITIONLEGS TAG_CLOSE
                  ( legelement)*
                  TAG_END_OPEN TRANSITIONLEGS TAG_CLOSE;
*/