package comp;

import elem.Airport;
import elem.ApproachLights;
import elem.BlastPad;
import elem.Com;
import elem.Dme;
import elem.Fuel;
import elem.GlideSlope;
import elem.Helipad;
import elem.Ils;
import elem.Lights;
import elem.Markings;
import elem.OffsetThreshold;
import elem.Overrun;
import elem.Runway;
import elem.RunwayStart;
import elem.Services;
import elem.TaxiName;
import elem.TaxiwayParking;
import elem.TaxiwayPath;
import elem.TaxiwayPoint;
import elem.Tower;
import elem.Vasi;
import elem.VisualModel;
import java.util.ArrayList;
import java.util.HashMap;

public class XML2SDLListener extends XML2SDLParserBaseListener {

    //ERROR Counter
    private int errorCount = 0;

    //Airport list =========================
    private ArrayList<Airport> airportList;

    //TEMP =================================
    //Airport
    private Airport airportElem;

    //TaxiwayPoint
    private TaxiwayPoint twPoint;
    private HashMap<Integer, TaxiwayPoint> twPointMap;

    //Services
    private Services servicesElem;
    private Fuel fuelElem;

    //Tower
    private Tower towerElem;

    //TaxiwayParking
    private TaxiwayParking twParking;
    private HashMap<Integer, TaxiwayParking> twParkingMap;

    //TaxiwayPath
    private TaxiwayPath twPath;
    private ArrayList<TaxiwayPath> twPathList;

    //Helipad
    private Helipad helipadElem;
    private ArrayList<Helipad> helipadList;

    //COM
    private Com comElem;
    private ArrayList<Com> comList;

    //Taxiname
    private TaxiName taxiNameElem;
    private HashMap<Integer, TaxiName> taxiNameMap;

    //Runway ============================
    private Runway runwayElem;
    private ArrayList<Runway> runwayList;

    //Markings
    private Markings markingsElem;

    //Lights
    private Lights lightsElem;

    //OffsetThreshold
    private OffsetThreshold offSetThresholdElem;

    //BlastPad
    private BlastPad blastPadElem;

    //Overrun
    private Overrun overRunElem;

    //ApproachLights
    private ApproachLights approachLightsElem;

    //Vasi
    private Vasi vasiElem;

    //RunwayStart
    private RunwayStart runwayStartElem;

    //ILS
    private Ils ilsElem;
    private GlideSlope glideSlopeElem;
    private Dme dmeElem;
    private VisualModel visualModelElem;

    //============================
    /**
     *
     */
    public XML2SDLListener() {
        airportList = new ArrayList<>();
    }

    /**
     * 
     * @return 
     */
    public ArrayList<Airport> getAirportList() {
        return airportList;
    }

    /**
     * 
     * @return 
     */
    public int getErrorCount() {
        return errorCount;
    }

    //UNIQUE INDEXES ====================================
    /**
     *
     * @param input
     * @throws IllegalArgumentException
     */
    private void checkUniqueIndexTWPoint(String input) throws IllegalArgumentException {

        String temp = input.replaceAll("^\"|\"$", "");
        int index = Integer.parseInt(temp);

        if (twPointMap.containsKey(index)) {
            throw new IllegalArgumentException("Taxiway Point - Index must be unique.");
        }
    }

    /**
     *
     * @param input
     * @throws IllegalArgumentException
     */
    private void checkUniqueIndexTWParking(String input) throws IllegalArgumentException {

        String temp = input.replaceAll("^\"|\"$", "");
        int index = Integer.parseInt(temp);

        if (twParkingMap.containsKey(index)) {
            throw new IllegalArgumentException("Taxiway Parking - Index must be unique.");
        }
    }

    /**
     *
     * @param input
     * @throws IllegalArgumentException
     */
    private void IndexExists(String input) throws IllegalArgumentException {
        String temp = input.replaceAll("^\"|\"$", "");
        int index = Integer.parseInt(temp);

        if (twPointMap.containsKey(index)) {
            return;
        }

        if (twParkingMap.containsKey(index)) {
            return;
        }

        throw new IllegalArgumentException("Index doesn't belong to an Taxiway Point or Taxiway Parking.");
    }

    // Document Element =================================
    @Override
    public void exitDocument(XML2SDLParser.DocumentContext ctx) {
        System.out.println("### Semantic Analysis ended ### " + System.lineSeparator());
    }

    // Airport Element =================================
    @Override
    public void enterAirportStart(XML2SDLParser.AirportStartContext ctx) {

        //Initialize new airport for processing
        airportElem = new Airport();

        //Initialize TaxiwayPoint map
        twPointMap = new HashMap<>();

        //Initialize Services
        servicesElem = new Services();

        //Initialize Tower
        towerElem = new Tower();

        //Initialize TaxiwayParking map
        twParkingMap = new HashMap<>();

        //Initialize Taxiway Path
        twPathList = new ArrayList<>();

        //Initialize Runway
        runwayList = new ArrayList<>();

        //Initialize Helipad
        helipadList = new ArrayList<>();

        //Initialize COM
        comList = new ArrayList<>();

        //Initialize TaxiName
        taxiNameMap = new HashMap<>();
    }

    @Override
    public void exitAirportStart(XML2SDLParser.AirportStartContext ctx) {
        try {
            airportElem.checkMissingAttr();
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.AIRPORT_TAG_START().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }
    }

    @Override
    public void exitAirportEnd(XML2SDLParser.AirportEndContext ctx) {

        //Add Taxiname
        airportElem.setTaxiNameMap(taxiNameMap);

        //Add COM
        airportElem.setComList(comList);

        //Add Helipad
        airportElem.setHelipadList(helipadList);

        //Add Runway
        airportElem.setRunwayList(runwayList);

        //Add TaxiwayPath
        airportElem.setTwPathList(twPathList);

        //Add TaxiwayParking 
        airportElem.setTwParkingMap(twParkingMap);

        //Add Tower
        airportElem.setTowerElem(towerElem);

        //Add Services
        airportElem.setServicesElem(servicesElem);

        //Add TaxiwayPoints to Airport
        airportElem.setTwPointMap(twPointMap);

        //Add processed airport to list
        airportList.add(airportElem);
    }

    @Override
    public void enterAirportExpReal(XML2SDLParser.AirportExpRealContext ctx) {

        try {
            String attr = ctx.airportAttrReal().getText();
            String value = ctx.ATTR_VALUE_REAL().getText();

            switch (attr) {
                case "lat":
                    airportElem.setLat(value);
                    break;
                case "lon":
                    airportElem.setLon(value);
                    break;
                case "magvar":
                    airportElem.setMagvar(value);
                    break;
                case "trafficScalar":
                    airportElem.setTrafficScalar(value);
                    break;
                default:
                    break;
            }
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.ATTR_EQ().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }
    }

    @Override
    public void enterAirportExpInt(XML2SDLParser.AirportExpIntContext ctx) {

        try {
            String attr = ctx.airportAttrInt().getText();
            String value = ctx.ATTR_VALUE_INT().getText();

            switch (attr) {
                case "magvar":
                    airportElem.setMagvar(value);
                    break;
                default:
                    break;
            }
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.ATTR_EQ().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }
    }

    @Override
    public void enterAirportExpStr(XML2SDLParser.AirportExpStrContext ctx) {

        try {
            String attr = ctx.airportAttrStr().getText();
            String value = ctx.ATTR_VALUE_STR().getText();

            switch (attr) {
                case "region":
                    airportElem.setRegion(value);
                    break;
                case "country":
                    airportElem.setCountry(value);
                    break;
                case "state":
                    airportElem.setState(value);
                    break;
                case "city":
                    airportElem.setCity(value);
                    break;
                case "name":
                    airportElem.setName(value);
                    break;
                case "alt":
                    airportElem.setAlt(value);
                    break;
                case "ident":
                    airportElem.setIdent(value);
                    break;
                case "airportTestRadius":
                    airportElem.setAirportTestRadius(value);
                    break;
                default:
                    break;
            }
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.ATTR_EQ().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }

    }

    // TaxiwayPoint Element =================================
    @Override
    public void enterTaxiwaypointElem(XML2SDLParser.TaxiwaypointElemContext ctx) {
        twPoint = new TaxiwayPoint();
    }

    @Override
    public void exitTaxiwaypointElem(XML2SDLParser.TaxiwaypointElemContext ctx) {
        twPointMap.put(twPoint.getIndex(), twPoint);
    }

    @Override
    public void enterTaxiwayPointExpReal(XML2SDLParser.TaxiwayPointExpRealContext ctx) {

        try {
            String attr = ctx.taxiwayPointAttReal().getText();
            String value = ctx.ATTR_VALUE_REAL().getText();

            switch (attr) {
                case "biasX":
                    twPoint.setBiasX(value);
                    break;
                case "biasZ":
                    twPoint.setBiasZ(value);
                    break;
                case "lon":
                    twPoint.setLon(value);
                    break;
                case "lat":
                    twPoint.setLat(value);
                    break;
                default:
                    break;
            }
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.ATTR_EQ().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }

    }

    @Override
    public void enterTaxiwayPointExpInt(XML2SDLParser.TaxiwayPointExpIntContext ctx) {

        try {
            String attr = ctx.taxiwayPointAttInt().getText();
            String value = ctx.ATTR_VALUE_INT().getText();

            switch (attr) {
                case "index":
                    checkUniqueIndexTWPoint(value);
                    twPoint.setIndex(value);
                    break;
                default:
                    break;
            }
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.ATTR_EQ().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }

    }

    @Override
    public void enterTaxiwayPointExpStr(XML2SDLParser.TaxiwayPointExpStrContext ctx) {
        try {
            String attr = ctx.taxiwayPointAttStr().getText();
            String value = ctx.ATTR_VALUE_STR().getText();

            switch (attr) {
                case "type":
                    twPoint.setType(value);
                    break;
                case "orientation":
                    twPoint.setOrientation(value);
                    break;
                default:
                    break;
            }
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.ATTR_EQ().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }
    }

    // Services Element =================================
    @Override
    public void enterFuelElem(XML2SDLParser.FuelElemContext ctx) {
        fuelElem = new Fuel();
    }

    @Override
    public void exitFuelElem(XML2SDLParser.FuelElemContext ctx) {

        try {
            fuelElem.checkMissingAttr();
            servicesElem.addFuel(fuelElem);
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.FUEL().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }

    }

    @Override
    public void enterFuelExpStr(XML2SDLParser.FuelExpStrContext ctx) {
        try {
            String attr = ctx.fuelAttrStr().getText();
            String value = ctx.ATTR_VALUE_STR().getText();

            switch (attr) {
                case "type":
                    fuelElem.setType(value);
                    break;
                case "availability":
                    fuelElem.setAvailability(value);
                    break;
                default:
                    break;
            }
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.ATTR_EQ().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }
    }

    @Override
    public void enterFuelExpInt(XML2SDLParser.FuelExpIntContext ctx) {
        try {
            String attr = ctx.TYPE().getText();
            String value = ctx.ATTR_VALUE_INT().getText();
            fuelElem.setType(value);
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.ATTR_EQ().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }
    }

    // Tower Element =================================
    @Override
    public void enterTowerExpReal(XML2SDLParser.TowerExpRealContext ctx) {
        try {
            String attr = ctx.towerAttrReal().getText();
            String value = ctx.ATTR_VALUE_REAL().getText();

            switch (attr) {
                case "lat":
                    towerElem.setLat(value);
                    break;
                case "lon":
                    towerElem.setLon(value);
                    break;
                default:
                    break;
            }
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.ATTR_EQ().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }
    }

    @Override
    public void enterTowerExpStr(XML2SDLParser.TowerExpStrContext ctx) {
        try {
            String attr = ctx.towerAttrStr().getText();
            String value = ctx.ATTR_VALUE_STR().getText();

            switch (attr) {
                case "alt":
                    towerElem.setAlt(value);
                    break;
                default:
                    break;
            }
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.ATTR_EQ().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }
    }

    // TaxiwayParking Element =================================
    @Override
    public void enterTaxiwayparkingElem(XML2SDLParser.TaxiwayparkingElemContext ctx) {
        twParking = new TaxiwayParking();
    }

    @Override
    public void exitTaxiwayparkingElem(XML2SDLParser.TaxiwayparkingElemContext ctx) {

        try {
            twParking.checkMissingAttr();
            twParkingMap.put(twParking.getIndex(), twParking);
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.TAXIWAYPARKING().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }

    }

    @Override
    public void enterTaxiwayparkingExpInt(XML2SDLParser.TaxiwayparkingExpIntContext ctx) {
        try {
            String attr = ctx.taxiwayparkingAttrInt().getText();
            String value = ctx.ATTR_VALUE_INT().getText();

            switch (attr) {
                case "index":
                    checkUniqueIndexTWParking(value);
                    twParking.setIndex(value);
                    break;
                case "number":
                    twParking.setNumber(value);
                    break;
                case "heading":
                    twParking.setHeading(value);
                    break;
                default:
                    break;
            }
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.ATTR_EQ().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }
    }

    @Override
    public void enterTaxiwayparkingExpReal(XML2SDLParser.TaxiwayparkingExpRealContext ctx) {
        try {
            String attr = ctx.taxiwayparkingAttrReal().getText();
            String value = ctx.ATTR_VALUE_REAL().getText();

            switch (attr) {
                case "lat":
                    twParking.setLat(value);
                    break;
                case "lon":
                    twParking.setLon(value);
                    break;
                case "biasX":
                    twParking.setBiasX(value);
                    break;
                case "biasZ":
                    twParking.setBiasZ(value);
                    break;
                case "heading":
                    twParking.setHeading(value);
                    break;
                case "radius":
                    twParking.setRadius(value);
                    break;
                case "teeOffset1":
                    twParking.setTeeOffset1(value);
                    break;
                case "teeOffset2":
                    twParking.setTeeOffset2(value);
                    break;
                case "teeOffset3":
                    twParking.setTeeOffset3(value);
                    break;
                case "teeOffset4":
                    twParking.setTeeOffset4(value);
                    break;
                default:
                    break;
            }
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.ATTR_EQ().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }
    }

    @Override
    public void enterTaxiwayparkingExpStr(XML2SDLParser.TaxiwayparkingExpStrContext ctx) {
        try {
            String attr = ctx.taxiwayparkingAttrStr().getText();
            String value = ctx.ATTR_VALUE_STR().getText();

            switch (attr) {
                case "type":
                    twParking.setType(value);
                    break;
                case "name":
                    twParking.setName(value);
                    break;
                case "airlineCodes":
                    twParking.setAirlineCodes(value);
                    break;
                case "pushBack":
                    twParking.setPushBack(value);
                    break;
                case "radius":
                    twParking.setRadius(value);
                    break;
                default:
                    break;
            }
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.ATTR_EQ().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }
    }

    // TaxiwayPath Element =================================
    @Override
    public void enterTaxiwayPathElem(XML2SDLParser.TaxiwayPathElemContext ctx) {
        twPath = new TaxiwayPath();
    }

    @Override
    public void exitTaxiwayPathElem(XML2SDLParser.TaxiwayPathElemContext ctx) {

        try {
            twPath.checkMissingAttr();
            twPathList.add(twPath);
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.TAXIWAYPATH().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }

    }

    @Override
    public void enterTaxiwayPathExpInt(XML2SDLParser.TaxiwayPathExpIntContext ctx) {
        try {
            String attr = ctx.taxiwayPathAttrInt().getText();
            String value = ctx.ATTR_VALUE_INT().getText();

            switch (attr) {
                case "start":
                    IndexExists(value);
                    twPath.setStart(value);
                    break;
                case "end":
                    IndexExists(value);
                    twPath.setEnd(value);
                    break;
                case "name":
                    twPath.setName(value);
                    break;
                case "weightLimit":
                    twPath.setWeightLimit(value);
                    break;
                case "number":
                    twPath.setNumber(value);
                    break;
                default:
                    break;
            }
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.ATTR_EQ().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }
    }

    @Override
    public void enterTaxiwayPathExpReal(XML2SDLParser.TaxiwayPathExpRealContext ctx) {
        try {
            String attr = ctx.taxiwayPathAttrReal().getText();
            String value = ctx.ATTR_VALUE_REAL().getText();

            switch (attr) {
                case "width":
                    twPath.setWidth(value);
                    break;
                case "weightLimit":
                    twPath.setWeightLimit(value);
                    break;
                default:
                    break;
            }

        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.ATTR_EQ().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }
    }

    @Override
    public void enterTaxiwayPathExpStr(XML2SDLParser.TaxiwayPathExpStrContext ctx) {
        try {
            String attr = ctx.taxiwayPathAttrString().getText();
            String value = ctx.ATTR_VALUE_STR().getText();

            switch (attr) {
                case "type":
                    twPath.setType(value);
                    break;
                case "surface":
                    twPath.setSurface(value);
                    break;
                case "drawSurface":
                    twPath.setDrawSurface(value);
                    break;
                case "drawDetail":
                    twPath.setDrawDetail(value);
                    break;
                case "centerLine":
                    twPath.setCenterLine(value);
                    break;
                case "centerLineLighted":
                    twPath.setCenterLineLighted(value);
                    break;
                case "leftEdge":
                    twPath.setLeftEdge(value);
                    break;
                case "leftEdgeLighted":
                    twPath.setLeftEdgeLighted(value);
                    break;
                case "rightEdge":
                    twPath.setRightEdge(value);
                    break;
                case "rightEdgeLighted":
                    twPath.setRightEdgeLighted(value);
                    break;
                case "number":
                    twPath.setNumber(value);
                    break;
                case "designator":
                    twPath.setDesignator(value);
                    break;
                case "width":
                    twPath.setWidth(value);
                    break;
                default:
                    break;
            }

        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.ATTR_EQ().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }
    }

    // Helipad Element =================================
    @Override
    public void enterHelipadElem(XML2SDLParser.HelipadElemContext ctx) {
        helipadElem = new Helipad();
    }

    @Override
    public void exitHelipadElem(XML2SDLParser.HelipadElemContext ctx) {
        try {
            helipadElem.checkMissingAttr();
            helipadList.add(helipadElem);
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.HELIPAD().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }
    }

    @Override
    public void enterHelipadExpStr(XML2SDLParser.HelipadExpStrContext ctx) {
        try {
            String attr = ctx.helipadAttStr().getText();
            String value = ctx.ATTR_VALUE_STR().getText();

            switch (attr) {
                case "lat":
                    helipadElem.setLat(value);
                    break;
                case "lon":
                    helipadElem.setLon(value);
                    break;
                case "alt":
                    helipadElem.setAlt(value);
                    break;
                case "surface":
                    helipadElem.setSurface(value);
                    break;
                case "length":
                    helipadElem.setLength(value);
                    break;
                case "width":
                    helipadElem.setWidth(value);
                    break;
                case "type":
                    helipadElem.setType(value);
                    break;
                case "closed":
                    helipadElem.setClosed(value);
                    break;
                case "transparent":
                    helipadElem.setTransparent(value);
                    break;
                default:
                    break;
            }
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.ATTR_EQ().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }
    }

    @Override
    public void enterHelipadExpInt(XML2SDLParser.HelipadExpIntContext ctx) {
        try {
            String attr = ctx.helipadAttInt().getText();
            String value = ctx.ATTR_VALUE_INT().getText();

            switch (attr) {
                case "heading":
                    helipadElem.setHeading(value);
                    break;
                case "length":
                    helipadElem.setLength(value);
                    break;
                case "width":
                    helipadElem.setWidth(value);
                    break;
                default:
                    break;
            }
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.ATTR_EQ().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }
    }

    @Override
    public void enterHelipadExpReal(XML2SDLParser.HelipadExpRealContext ctx) {
        try {
            String attr = ctx.helipadAttReal().getText();
            String value = ctx.ATTR_VALUE_REAL().getText();

            switch (attr) {
                case "red":
                    helipadElem.setRed(value);
                    break;
                case "green":
                    helipadElem.setGreen(value);
                    break;
                case "blue":
                    helipadElem.setBlue(value);
                    break;
                case "heading":
                    helipadElem.setHeading(value);
                    break;
                case "lat":
                    helipadElem.setLat(value);
                    break;
                case "lon":
                    helipadElem.setLon(value);
                    break;
                default:
                    break;
            }
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.ATTR_EQ().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }
    }

    // Com Element =================================
    @Override
    public void enterComElem(XML2SDLParser.ComElemContext ctx) {
        comElem = new Com();
    }

    @Override
    public void exitComElem(XML2SDLParser.ComElemContext ctx) {
        try {
            comElem.checkMissingAttr();
            comList.add(comElem);
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.COM().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }
    }

    @Override
    public void enterComExpStr(XML2SDLParser.ComExpStrContext ctx) {
        try {
            String attr = ctx.comAttrStr().getText();
            String value = ctx.ATTR_VALUE_STR().getText();

            switch (attr) {
                case "type":
                    comElem.setType(value);
                    break;
                case "name":
                    comElem.setName(value);
                    break;
                default:
                    break;
            }
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.ATTR_EQ().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }
    }

    @Override
    public void enterComExpReal(XML2SDLParser.ComExpRealContext ctx) {
        try {
            String attr = ctx.FREQUENCY().getText();
            String value = ctx.ATTR_VALUE_REAL().getText();

            switch (attr) {
                case "frequency":
                    comElem.setFrequency(value);
                    break;
                default:
                    break;
            }
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.ATTR_EQ().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }
    }

    // TaxiName Element =================================
    @Override
    public void enterTaxiNameElem(XML2SDLParser.TaxiNameElemContext ctx) {
        taxiNameElem = new TaxiName();
    }

    @Override
    public void exitTaxiNameElem(XML2SDLParser.TaxiNameElemContext ctx) {

        try {
            taxiNameElem.checkMissingAttr();
            taxiNameMap.put(taxiNameElem.getIndex(), taxiNameElem);
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.TAXINAME().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }

    }

    @Override
    public void enterTaxiNameExpInt(XML2SDLParser.TaxiNameExpIntContext ctx) {
        try {
            String attr = ctx.INDEX().getText();
            String value = ctx.ATTR_VALUE_INT().getText();

            switch (attr) {
                case "index":
                    IndexExists(value);
                    taxiNameElem.setIndex(value);
                    break;
                default:
                    break;
            }
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.ATTR_EQ().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }
    }

    @Override
    public void enterTaxiNameExpStr(XML2SDLParser.TaxiNameExpStrContext ctx) {
        try {
            String attr = ctx.NAME().getText();
            String value = ctx.ATTR_VALUE_STR().getText();

            switch (attr) {
                case "name":
                    taxiNameElem.setName(value);
                    break;
                default:
                    break;
            }
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.ATTR_EQ().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }
    }

    // Runway Element =================================
    @Override
    public void enterRunwayElemStart(XML2SDLParser.RunwayElemStartContext ctx) {
        runwayElem = new Runway();
    }

    @Override
    public void exitRunwayElemStart(XML2SDLParser.RunwayElemStartContext ctx) {
        try {
            runwayElem.checkMissingAttr();
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.RUNWAY().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }
    }

    @Override
    public void enterRunwayElemClose(XML2SDLParser.RunwayElemCloseContext ctx) {
        try {
            runwayList.add(runwayElem);
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.RUNWAY().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }
    }

    @Override
    public void enterRunwayExpString(XML2SDLParser.RunwayExpStringContext ctx) {
        try {
            String attr = ctx.runwayAttString().getText();
            String value = ctx.ATTR_VALUE_STR().getText();

            switch (attr) {
                case "alt":
                    runwayElem.setAlt(value);
                    break;
                case "surface":
                    runwayElem.setSurface(value);
                    break;
                case "length":
                    runwayElem.setLength(value);
                    break;
                case "width":
                    runwayElem.setWidth(value);
                    break;
                case "number":
                    runwayElem.setNumber(value);
                    break;
                case "designator":
                    runwayElem.setDesignator(value);
                    break;
                case "primaryDesignator":
                    runwayElem.setPrimDesignator(value);
                    break;
                case "secondaryDesignator":
                    runwayElem.setSecondDesignator(value);
                    break;
                case "patternAltitude":
                    runwayElem.setPatternAltitude(value);
                    break;
                case "primaryTakeoff":
                    runwayElem.setPrimaryTakeOff(value);
                    break;
                case "primaryLanding":
                    runwayElem.setPrimaryLanding(value);
                    break;
                case "primaryPattern":
                    runwayElem.setPrimaryPattern(value);
                    break;
                case "secondaryTakeoff":
                    runwayElem.setSecondTakeOff(value);
                    break;
                case "secondaryLanding":
                    runwayElem.setSecondLanding(value);
                    break;
                case "secondaryPattern":
                    runwayElem.setSecondPattern(value);
                    break;
                case "primaryMarkingBias":
                    runwayElem.setPrimaryMarkingBias(value);
                    break;
                case "secondaryMarkingBias":
                    runwayElem.setSecondMarkingBias(value);
                    break;
                default:
                    break;
            }
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.ATTR_EQ().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }
    }

    @Override
    public void enterRunwayExpReal(XML2SDLParser.RunwayExpRealContext ctx) {
        try {
            String attr = ctx.runwayAttReal().getText();
            String value = ctx.ATTR_VALUE_REAL().getText();

            switch (attr) {
                case "lat":
                    runwayElem.setLat(value);
                    break;
                case "lon":
                    runwayElem.setLon(value);
                    break;
                case "heading":
                    runwayElem.setHeading(value);
                    break;
                case "alt":
                    runwayElem.setAlt(value);
                    break;
                case "number":
                    runwayElem.setNumber(value);
                    break;
                case "designator":
                    runwayElem.setDesignator(value);
                    break;
                case "primaryDesignator":
                    runwayElem.setPrimDesignator(value);
                    break;
                case "secondaryDesignator":
                    runwayElem.setSecondDesignator(value);
                    break;
                case "patternAltitude ":
                    runwayElem.setPatternAltitude(value);
                    break;
                default:
                    break;
            }
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.ATTR_EQ().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }
    }

    @Override
    public void enterRunwayExpInt(XML2SDLParser.RunwayExpIntContext ctx) {
        try {
            String attr = ctx.runwayAttInt().getText();
            String value = ctx.ATTR_VALUE_INT().getText();

            switch (attr) {
                case "number":
                    runwayElem.setNumber(value);
                    break;
                case "heading":
                    runwayElem.setHeading(value);
                    break;
                default:
                    break;
            }
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.ATTR_EQ().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }
    }

    // Markings Element =================================
    @Override
    public void enterMarkingsElem(XML2SDLParser.MarkingsElemContext ctx) {
        markingsElem = new Markings();
    }

    @Override
    public void exitMarkingsElem(XML2SDLParser.MarkingsElemContext ctx) {
        runwayElem.setMarkings(markingsElem);
    }

    @Override
    public void enterMarkingsExpBool(XML2SDLParser.MarkingsExpBoolContext ctx) {
        try {
            String attr = ctx.markingsAttsBool().getText();
            String value = ctx.ATTR_VALUE_STR().getText();

            switch (attr) {
                case "alternateThreshold":
                    markingsElem.setAlternateThreshold(value);
                    break;
                case "alternateTouchdown":
                    markingsElem.setAlternateTouchdown(value);
                    break;
                case "alternateFixedDistance":
                    markingsElem.setAlternateFixedDistance(value);
                    break;
                case "alternatePrecision":
                    markingsElem.setAlternatePrecision(value);
                    break;
                case "leadingZeroIdent":
                    markingsElem.setLeadingZeroIdent(value);
                    break;
                case "noThresholdEndArrows":
                    markingsElem.setNoThresholdEndArrows(value);
                    break;
                case "edges":
                    markingsElem.setEdges(value);
                    break;
                case "threshold":
                    markingsElem.setThreshold(value);
                    break;
                case "fixedDistance":
                    markingsElem.setFixedDistance(value);
                    break;
                case "touchdown":
                    markingsElem.setTouchdown(value);
                    break;
                case "dashes":
                    markingsElem.setDashes(value);
                    break;
                case "ident":
                    markingsElem.setIdent(value);
                    break;
                case "precision":
                    markingsElem.setPrecision(value);
                    break;
                case "edgePavement":
                    markingsElem.setEdgePavement(value);
                    break;
                case "singleEnd":
                    markingsElem.setSingleEnd(value);
                    break;
                case "primaryClosed":
                    markingsElem.setPrimaryClosed(value);
                    break;
                case "secondaryClosed":
                    markingsElem.setSecondaryClosed(value);
                    break;
                case "primaryStol":
                    markingsElem.setPrimaryStol(value);
                    break;
                case "secondaryStol":
                    markingsElem.setSecondaryStol(value);
                    break;
                default:
                    break;
            }
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.ATTR_EQ().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }
    }

    // Lights Element =================================
    @Override
    public void enterLightsElem(XML2SDLParser.LightsElemContext ctx) {
        lightsElem = new Lights();
    }

    @Override
    public void exitLightsElem(XML2SDLParser.LightsElemContext ctx) {

        try {
            lightsElem.checkMissingAttr();
            runwayElem.setLights(lightsElem);
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.LIGHTS().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }

    }

    @Override
    public void enterLightsExpStr(XML2SDLParser.LightsExpStrContext ctx) {
        try {
            String attr = ctx.lightsAttStr().getText();
            String value = ctx.ATTR_VALUE_STR().getText();

            switch (attr) {
                case "center":
                    lightsElem.setCenter(value);
                    break;
                case "edge":
                    lightsElem.setEdge(value);
                    break;
                case "centerRed":
                    lightsElem.setCenterRed(value);
                    break;
                default:
                    break;
            }
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.ATTR_EQ().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }
    }

    // OffsetThreshold Element =================================
    @Override
    public void enterOffsetThresholdElem(XML2SDLParser.OffsetThresholdElemContext ctx) {
        offSetThresholdElem = new OffsetThreshold();
    }

    @Override
    public void exitOffsetThresholdElem(XML2SDLParser.OffsetThresholdElemContext ctx) {

        try {
            offSetThresholdElem.checkMissingAttr();
            runwayElem.setOffSetThreshold(offSetThresholdElem);
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.OFFSETTHRESHOLD().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }

    }

    @Override
    public void enterOffsetThresholdExpStr(XML2SDLParser.OffsetThresholdExpStrContext ctx) {
        try {
            String attr = ctx.offsetThresholdAttStr().getText();
            String value = ctx.ATTR_VALUE_STR().getText();

            switch (attr) {
                case "end":
                    offSetThresholdElem.setEnd(value);
                    break;
                case "length":
                    offSetThresholdElem.setLength(value);
                    break;
                case "width":
                    offSetThresholdElem.setWidth(value);
                    break;
                case "surface":
                    offSetThresholdElem.setSurface(value);
                    break;
                default:
                    break;
            }
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.ATTR_EQ().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }
    }

    // BlastPad Element =================================
    @Override
    public void enterBlastPadElem(XML2SDLParser.BlastPadElemContext ctx) {
        blastPadElem = new BlastPad();
    }

    @Override
    public void exitBlastPadElem(XML2SDLParser.BlastPadElemContext ctx) {
        try {
            blastPadElem.checkMissingAttr();
            runwayElem.setBlastPad(blastPadElem);
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.BLASTPAD().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }

    }

    @Override
    public void enterBlastPadExpStr(XML2SDLParser.BlastPadExpStrContext ctx) {
        try {
            String attr = ctx.blastPadAttStr().getText();
            String value = ctx.ATTR_VALUE_STR().getText();

            switch (attr) {
                case "end":
                    blastPadElem.setEnd(value);
                    break;
                case "length":
                    blastPadElem.setLength(value);
                    break;
                case "width":
                    blastPadElem.setWidth(value);
                    break;
                case "surface":
                    blastPadElem.setSurface(value);
                    break;
                default:
                    break;
            }
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.ATTR_EQ().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }
    }

    // Overrun Element =================================
    @Override
    public void enterOverrunElem(XML2SDLParser.OverrunElemContext ctx) {
        overRunElem = new Overrun();
    }

    @Override
    public void exitOverrunElem(XML2SDLParser.OverrunElemContext ctx) {

        try {
            overRunElem.checkMissingAttr();
            runwayElem.setOverRun(overRunElem);
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.OVERRUN().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }

    }

    @Override
    public void enterOverrunExpStr(XML2SDLParser.OverrunExpStrContext ctx) {
        try {
            String attr = ctx.overrunAttStr().getText();
            String value = ctx.ATTR_VALUE_STR().getText();

            switch (attr) {
                case "end":
                    overRunElem.setEnd(value);
                    break;
                case "length":
                    overRunElem.setLength(value);
                    break;
                case "width":
                    overRunElem.setWidth(value);
                    break;
                case "surface":
                    overRunElem.setSurface(value);
                    break;
                default:
                    break;
            }
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.ATTR_EQ().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }
    }

    // Approach Lights Element =================================
    @Override
    public void enterApproachLightsElem(XML2SDLParser.ApproachLightsElemContext ctx) {
        approachLightsElem = new ApproachLights();
    }

    @Override
    public void exitApproachLightsElem(XML2SDLParser.ApproachLightsElemContext ctx) {
        try {
            approachLightsElem.checkMissingAttr();
            runwayElem.setApproachLight(approachLightsElem);
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.APPROACHLIGHTS().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }
    }

    @Override
    public void enterApproachLightsExpInt(XML2SDLParser.ApproachLightsExpIntContext ctx) {
        try {
            String attr = ctx.STROBES().getText();
            String value = ctx.ATTR_VALUE_INT().getText();

            switch (attr) {
                case "strobes":
                    approachLightsElem.setStrobes(value);
                    break;
                default:
                    break;
            }
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.ATTR_EQ().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }
    }

    @Override
    public void enterApproachLightsExpStr(XML2SDLParser.ApproachLightsExpStrContext ctx) {
        try {
            String attr = ctx.approachLightsAttStr().getText();
            String value = ctx.ATTR_VALUE_STR().getText();

            switch (attr) {
                case "end":
                    approachLightsElem.setEnd(value);
                    break;
                case "system":
                    approachLightsElem.setSystem(value);
                    break;
                case "reil":
                    approachLightsElem.setReil(value);
                    break;
                case "endLights":
                    approachLightsElem.setEndLights(value);
                    break;
                case "touchdown":
                    approachLightsElem.setTouchdown(value);
                    break;
                default:
                    break;
            }
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.ATTR_EQ().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }
    }

    // Vasi Element =================================
    @Override
    public void enterVasiElem(XML2SDLParser.VasiElemContext ctx) {
        vasiElem = new Vasi();
    }

    @Override
    public void exitVasiElem(XML2SDLParser.VasiElemContext ctx) {
        try {
            vasiElem.checkMissingAttr();
            runwayElem.setVasi(vasiElem);
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.VASI().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }

    }

    @Override
    public void enterVasiExpStr(XML2SDLParser.VasiExpStrContext ctx) {
        try {
            String attr = ctx.vasiAttStr().getText();
            String value = ctx.ATTR_VALUE_STR().getText();

            switch (attr) {
                case "end":
                    vasiElem.setEnd(value);
                    break;
                case "type":
                    vasiElem.setType(value);
                    break;
                case "side":
                    vasiElem.setSide(value);
                    break;
                case "biasX":
                    vasiElem.setBiasX(value);
                    break;
                case "biasZ":
                    vasiElem.setBiasZ(value);
                    break;
                case "spacing":
                    vasiElem.setSpacing(value);
                    break;
                case "pitch":
                    vasiElem.setPitch(value);
                    break;
                default:
                    break;
            }
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.ATTR_EQ().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }
    }

    @Override
    public void enterVasiExpInt(XML2SDLParser.VasiExpIntContext ctx) {
        try {
            String attr = ctx.PITCH().getText();
            String value;

            if (ctx.ATTR_VALUE_INT() != null) {
                value = ctx.ATTR_VALUE_INT().getText();
            } else {
                value = ctx.ATTR_VALUE_REAL().getText();
            }

            switch (attr) {
                case "pitch":
                    vasiElem.setPitch(value);
                    break;
                default:
                    break;
            }
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.ATTR_EQ().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }
    }

    // RunwayStart Element =================================
    @Override
    public void enterRunwayStartElem(XML2SDLParser.RunwayStartElemContext ctx) {
        runwayStartElem = new RunwayStart();
    }

    @Override
    public void exitRunwayStartElem(XML2SDLParser.RunwayStartElemContext ctx) {
        try {
            runwayStartElem.checkMissingAttr();
            runwayElem.setRunwayStart(runwayStartElem);
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.RUNWAYSTART().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }

    }

    @Override
    public void enterRunwayStartExpReal(XML2SDLParser.RunwayStartExpRealContext ctx) {
        try {
            String attr = ctx.runwayStartAttReal().getText();
            String value = ctx.ATTR_VALUE_REAL().getText();

            switch (attr) {
                case "lat":
                    runwayStartElem.setLat(value);
                    break;
                case "lon":
                    runwayStartElem.setLon(value);
                    break;
                case "alt":
                    runwayStartElem.setAlt(value);
                    break;
                case "heading":
                    runwayElem.setHeading(value);
                    break;
                default:
                    break;
            }
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.ATTR_EQ().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }
    }

    @Override
    public void enterRunwayStartExpStr(XML2SDLParser.RunwayStartExpStrContext ctx) {
        try {
            String attr = ctx.runwayStartAttStr().getText();
            String value = ctx.ATTR_VALUE_STR().getText();

            switch (attr) {
                case "type":
                    runwayStartElem.setType(value);
                    break;
                case "end":
                    runwayStartElem.setEnd(value);
                    break;
                default:
                    break;
            }
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.ATTR_EQ().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }
    }

    // ILS Element =================================
    @Override
    public void enterIlsElemStart(XML2SDLParser.IlsElemStartContext ctx) {
        ilsElem = new Ils();
    }

    @Override
    public void exitIlsElemClose(XML2SDLParser.IlsElemCloseContext ctx) {

        try {
            ilsElem.checkMissingAttr();
            runwayElem.setIls(ilsElem);

        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.ILS().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }

    }

    @Override
    public void enterIlsExpStr(XML2SDLParser.IlsExpStrContext ctx) {
        try {
            String attr = ctx.ilsAttStr().getText();
            String value = ctx.ATTR_VALUE_STR().getText();

            switch (attr) {
                case "name":
                    ilsElem.setName(value);
                    break;
                case "end":
                    ilsElem.setEnd(value);
                    break;
                case "ident":
                    ilsElem.setIdent(value);
                    break;
                case "backCourse":
                    ilsElem.setBackCourse(value);
                    break;
                case "alt":
                    ilsElem.setAlt(value);
                    break;
                case "range":
                    ilsElem.setRange(value);
                    break;
                default:
                    break;
            }
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.ATTR_EQ().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }
    }

    @Override
    public void enterIlsExpReal(XML2SDLParser.IlsExpRealContext ctx) {
        try {
            String attr = ctx.ilsAttReal().getText();
            String value = ctx.ATTR_VALUE_REAL().getText();

            switch (attr) {
                case "lat":
                    ilsElem.setLat(value);
                    break;
                case "lon":
                    ilsElem.setLon(value);
                    break;
                case "heading":
                    ilsElem.setHeading(value);
                    break;
                case "frequency":
                    ilsElem.setFrequency(value);
                    break;
                case "width":
                    ilsElem.setWidth(value);
                    break;
                case "magvar":
                    ilsElem.setMagvar(value);
                    break;
                default:
                    break;
            }
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.ATTR_EQ().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }
    }

    @Override
    public void enterIlsExpInt(XML2SDLParser.IlsExpIntContext ctx) {
        try {
            String attr = ctx.ilsAttInt().getText();
            String value = ctx.ATTR_VALUE_INT().getText();

            switch (attr) {
                case "magvar":
                    ilsElem.setMagvar(value);
                    break;
                case "heading":
                    ilsElem.setHeading(value);
                default:
                    break;
            }
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.ATTR_EQ().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }
    }

    // GlideSlop Element =================================
    @Override
    public void enterGlideSlopeElem(XML2SDLParser.GlideSlopeElemContext ctx) {
        glideSlopeElem = new GlideSlope();
    }

    @Override
    public void exitGlideSlopeElem(XML2SDLParser.GlideSlopeElemContext ctx) {

        try {
            glideSlopeElem.checkMissingAttr();
            ilsElem.setGlideSlope(glideSlopeElem);
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.GLIDESLOPE().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }

    }

    @Override
    public void enterGlideSlopeExpReal(XML2SDLParser.GlideSlopeExpRealContext ctx) {
        try {
            String attr = ctx.glideSlopeAttReal().getText();
            String value = ctx.ATTR_VALUE_REAL().getText();

            switch (attr) {
                case "lat":
                    glideSlopeElem.setLat(value);
                    break;
                case "lon":
                    glideSlopeElem.setLon(value);
                    break;
                case "alt":
                    glideSlopeElem.setAlt(value);
                    break;
                case "pitch":
                    glideSlopeElem.setPitch(value);
                    break;
                default:
                    break;
            }
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.ATTR_EQ().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }
    }

    @Override
    public void enterGlideSlopeExpStr(XML2SDLParser.GlideSlopeExpStrContext ctx) {
        try {
            String attr = ctx.glideSlopeAttStr().getText();
            String value = ctx.ATTR_VALUE_STR().getText();

            switch (attr) {
                case "lat":
                    glideSlopeElem.setLat(value);
                    break;
                case "lon":
                    glideSlopeElem.setLon(value);
                    break;
                case "alt":
                    glideSlopeElem.setAlt(value);
                    break;
                case "range":
                    glideSlopeElem.setRange(value);
                    break;
                default:
                    break;
            }
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.ATTR_EQ().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }
    }

    @Override
    public void enterGlideSlopeExpInt(XML2SDLParser.GlideSlopeExpIntContext ctx) {
        try {
            String attr = ctx.glideSlopeAttInt().getText();
            String value = ctx.ATTR_VALUE_INT().getText();

            switch (attr) {
                case "range":
                    glideSlopeElem.setRange(value);
                    break;
                case "pitch":
                    glideSlopeElem.setPitch(value);
                    break;
                default:
                    break;
            }
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.ATTR_EQ().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }
    }

    // Dme Element =================================
    @Override
    public void enterDmeElem(XML2SDLParser.DmeElemContext ctx) {
        dmeElem = new Dme();
    }

    @Override
    public void exitDmeElem(XML2SDLParser.DmeElemContext ctx) {
        try {
            dmeElem.checkMissingAttr();
            ilsElem.setDme(dmeElem);
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.DME().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }

    }

    @Override
    public void enterDmeExpReal(XML2SDLParser.DmeExpRealContext ctx) {
        try {
            String attr = ctx.dmeAttReal().getText();
            String value = ctx.ATTR_VALUE_REAL().getText();

            switch (attr) {
                case "lat":
                    dmeElem.setLat(value);
                    break;
                case "lon":
                    dmeElem.setLon(value);
                    break;
                case "alt":
                    dmeElem.setAlt(value);
                    break;
                default:
                    break;
            }
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.ATTR_EQ().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }
    }

    @Override
    public void enterDmeExpInt(XML2SDLParser.DmeExpIntContext ctx) {
        try {
            String attr = ctx.RANGE().getText();
            String value = ctx.ATTR_VALUE_INT().getText();

            switch (attr) {
                case "range":
                    dmeElem.setRange(value);
                    break;
                default:
                    break;
            }
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.ATTR_EQ().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }
    }

    @Override
    public void enterDmeExpStr(XML2SDLParser.DmeExpStrContext ctx) {
        try {
            String attr = ctx.dmeAttStr().getText();
            String value = ctx.ATTR_VALUE_STR().getText();

            switch (attr) {
                case "lat":
                    dmeElem.setLat(value);
                    break;
                case "lon":
                    dmeElem.setLon(value);
                    break;
                case "alt":
                    dmeElem.setAlt(value);
                    break;
                case "range":
                    dmeElem.setRange(value);
                    break;
                default:
                    break;
            }
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.ATTR_EQ().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }
    }

    // Visual Model Element =================================
    @Override
    public void enterVisualModelElemStart(XML2SDLParser.VisualModelElemStartContext ctx) {
        visualModelElem = new VisualModel();
    }

    @Override
    public void exitVisualModelElemClose(XML2SDLParser.VisualModelElemCloseContext ctx) {
        ilsElem.setVisualModel(visualModelElem);
    }

    @Override
    public void enterVisualModelExpReal(XML2SDLParser.VisualModelExpRealContext ctx) {
        try {
            String attr = ctx.HEADING().getText();
            String value = ctx.ATTR_VALUE_REAL().getText();

            switch (attr) {
                case "heading":
                    visualModelElem.setHeading(value);
                    break;
                default:
                    break;
            }
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.ATTR_EQ().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }
    }

    @Override
    public void enterVisualModelExpInt(XML2SDLParser.VisualModelExpIntContext ctx) {
        try {
            String attr = ctx.HEADING().getText();
            String value = ctx.ATTR_VALUE_INT().getText();

            switch (attr) {
                case "heading":
                    visualModelElem.setHeading(value);
                    break;
                default:
                    break;
            }
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.ATTR_EQ().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }
    }

    @Override
    public void enterVisualModelExpStr(XML2SDLParser.VisualModelExpStrContext ctx) {
        try {
            String attr = ctx.visualModelAttStr().getText();
            String value = ctx.ATTR_VALUE_STR().getText();

            switch (attr) {
                case "name":
                    visualModelElem.setName(value);
                    break;
                case "imageComplexity ":
                    visualModelElem.setImageComplexity(value);
                    break;
                default:
                    break;
            }
        } catch (IllegalArgumentException e) {
            errorCount++;
            int errorLine = ctx.ATTR_EQ().getSymbol().getLine();
            System.err.println(errorLine + " - " + e.getMessage());
        }
    }
}
