lexer grammar XML2SDLLexer;

//=====================================
//LEXER
//=====================================

OUT_DATA: (~'<')+ -> skip;
OUT_START: '<' -> skip;

AIRPORT_TAG_START: '<Airport' -> pushMode(AIRPORT_MODE);

// -------------- AIRPORT MODE ----------------
mode AIRPORT_MODE;

TAG_START_OPEN : '<';
TAG_END_OPEN : '</';
TAG_CLOSE : '>';
TAG_EMPTY_CLOSE : '/>';

//Attribute values
ATTR_EQ : '=';
ATTR_DQ : '"';
ATTR_QU : '\'';

//TAGS
//AIRPORT : 'Airport';
AIRPORT_TAG_CLOSE: 'Airport>' -> popMode;
TAXIWAYPOINT : 'TaxiwayPoint';
SERVICES : 'Services';
FUEL : 'Fuel';
TOWER : 'Tower';
TAXIWAYPARKING : 'TaxiwayParking';
ROUTE : 'Route';
PREVIOUS : 'Previous';
NEXT : 'Next';
TAXIWAYPATH: 'TaxiwayPath';
RUNWAY_ALIAS: 'RunwayAlias';
COM: 'Com';
TAXINAME : 'TaxiName';
RUNWAY: 'Runway';
MARKINGS : 'Markings';
LIGHTS: 'Lights';
OFFSETTHRESHOLD: 'OffsetThreshold';
BLASTPAD: 'BlastPad';
OVERRUN : 'Overrun';
APPROACHLIGHTS : 'ApproachLights';
VASI : 'Vasi';
ILS : 'Ils';
GLIDESLOPE : 'GlideSlope';
DME : 'Dme';
VISUALMODEL : 'VisualModel';
BiasXYZ : 'BiasXYZ';
RUNWAYSTART : 'RunwayStart';
HELIPAD : 'Helipad';
NDB: 'Ndb';
START_TAG: 'Start';
TAXIWAYSIGN: 'TaxiwaySign';
VERTEX: 'Vertex';
GEOPOL: 'Geopol';
MARKER: 'Marker';
WAYPOINT: 'Waypoint';
APPROACH: 'Approach';
APPROACHLEGS : 'ApproachLegs';
LEG : 'Leg';
MISSEDAPPROACHLEAGS : 'MissedApproachLegs';
TRANSITION : 'Transition';
DMEARC : 'DmeArc';
TRANSITIONLEGS: 'TransitionLegs';

//ATTRIBUTES
LAT : 'lat';
LON : 'lon';
ALT : 'alt';
TYPE : 'type';
INDEX : 'index';
BIASX:'biasX';
BIASZ:'biasZ';
NAME : 'name';
NUMBER: 'number';
IDENT : 'ident';
MAGVAR : 'magvar';
AIRPORT_TEST_RADIUS : 'airportTestRadius';
TRAFFIC_SCALAR : 'trafficScalar';
REGION : 'region';
COUNTRY : 'country';
STATE : 'state';
CITY : 'city';
ORIENTATION: 'orientation';
AVAILABILITY : 'availability';
HEADING : 'heading';
RADIUS : 'radius';
AIRLINECODES : 'airlineCodes';
PUSHBACK : 'pushBack';
TEEOFFSET1 : 'teeOffset1';
TEEOFFSET2 : 'teeOffset2';
TEEOFFSET3 : 'teeOffset3';
TEEOFFSET4 : 'teeOffset4';
ROUTETYPE : 'routeType';
WAYPOINTTYPE : 'waypointType';
WAYPOINTREGION : 'waypointRegion';
WAYPOINTIDENT : 'waypointIdent';
ALTITUDEMINIMUN : 'altitudeMinimum';
START : 'start';
END : 'end';
WIDTH : 'width';
WEIGHT_LIMIT : 'weightLimit';
SURFACE : 'surface';
DRAW_SURFACE : 'drawSurface';
DRAW_DETAIL : 'drawDetail';
DESIGNATOR : 'designator';
CENTER_LINE : 'centerLine';
CENTER_LINE_LIGHTED : 'centerLineLighted';
LEFT_EDGE : 'leftEdge';
LEFT_EDGE_LIGHTED : 'leftEdgeLighted';
RIGHT_EDGE : 'rightEdge';
RIGHT_EDGE_LIGHTED : 'rightEdgeLighted';
FREQUENCY: 'frequency';
LENGTH: 'length';
PRIMARY_DESIGNATOR: 'primaryDesignator';
SECONDARY_DESIGNATOR: 'secondaryDesignator';
PATTERN_ALTITUDE: 'patternAltitude';
PRIMARY_TAKE_OFF: 'primaryTakeoff';
PRIMARY_LANDING: 'primaryLanding';
PRIMARY_PATTERN: 'primaryPattern';
SECONDARY_TAKE_OFF: 'secondaryTakeoff';
SECONDARY_LANDING: 'secondaryLanding';
SECONDARY_PATTERN: 'secondaryPattern';
PRIMARY_MARKING_BIAS: 'primaryMarkingBias';
SECONDARY_MARKING_BIAS: 'secondaryMarkingBias';
ALTERNATETHRESHOLD: 'alternateThreshold';
ALTERNATETOUCHDOWN: 'alternateTouchdown';
ALTERNATEFIXEDDISTANCE: 'alternateFixedDistance';
ALTERNATEPRECISION: 'alternatePrecision';
LEADINGZEROSIDENT: 'leadingZeroIdent';
NOTHRESHOLDENDARROWS: 'noThresholdEndArrows';
EDGES: 'edges';
THRESHOLD: 'threshold';
FIXED_DISTANCE: 'fixedDistance';
FIXED:'fixed';
TOUCHDOWN: 'touchdown';
DASHES: 'dashes';
PRECISION: 'precision';
EDGEPAVEMENT:'edgePavement';
SINGLEEND:'singleEnd';
PRIMARYCLOSE:'primaryClosed';
SECONDARYCLOSED:'secondaryClosed';
PRIMARYSTOL: 'primaryStol';
SECONDARYSTOL:'secondaryStol';
CENTER:'center';
EDGE:'edge';
CENTERRED:'centerRed';
SYSTEM: 'system';
REIL : 'reil';
ENDLIGHTS: 'endLights';
STROBES: 'strobes';
SIDE:'side';
SPACING:'spacing';
PITCH:'pitch';
RANGE: 'range';
BACKCOURSE: 'backCourse';
IMAGECOMPLEXITY: 'imageComplexity';
BIASY: 'biasY';
CLOSED: 'closed';
TRANSPARENT : 'transparent';
RED : 'red';
GREEN : 'green';
BLUE: 'blue';
LABEL: 'label';
SIZE: 'size';
JUSTIFICATION: 'justification';
FIXTYPE: 'fixType';
FIXREGION: 'fixRegion';
FIXIDENT: 'fixIdent';
ALTITUDE: 'altitude';
MISSALTITUDE: 'missedAltitude';
SUFFIX : 'suffix';
GPSOVERLAY : 'gpsOverlay';
FLYOVER: 'flyOver';
TURNDIRECTION: 'turnDirection';
RECOMMENDEDTYPE: 'recommendedType';
RECOMMENDEREGION: 'recommendedRegion';
RECOMMENDEIDENT: 'recommendedIdent';
THETA: 'theta';
RHO: 'rho';
TRUECOURSE: 'trueCourse';
MAGNETICCOURSE: 'magneticCourse';
DISTANCE: 'distance';
TIME: 'time';
ALTITUDEDESCRIPTOR: 'altitudeDescriptor';
ALTITUDE1: 'altitude1';
ALTITUDE2: 'altitude2';
TRANSITIONTYPE: 'transitionType';
RADIAL: 'radial';
DMELDENT: 'dmeIdent';
RUNWAY_ATTR : 'runway';

//SKIP TAGS
fragment SKIP_TAG: 'Aprons'
                   | 'Apron'
                   | 'ApronEdgeLights'
                   | 'TaxiwaySign'
                   | 'Waypoint'
                   | 'Approach'
                   | 'ApproachLegs'
                   | 'Leg'
                   | 'MissedApproachLegs'
                   | 'Transition'
                   | 'TransitionLegs'
                   | 'DmeArc'
                   | 'Ndb'
                   | 'Start'
                   | 'Jetway'
                   | 'SceneryObject'
                   | 'BlastFence'
                   | 'BoundaryFence'
                   | 'DeleteAirport'
                   | 'LibraryObject'
                   | 'EdgeLights'
                   | 'Geopol'
                   | 'Marker'
                   | 'Vertex'
                   | 'RunwayAlias'
                   | 'SceneryObject'
                   | 'LibraryObject'
                   | 'AttachedObject'
                   | 'RandomAttach'
                   | 'Beacon';

SKIP_TAG_ELEM: (('<' SKIP_TAG (~'>')* '>') 
               | ('</' SKIP_TAG '>')
               | ('<' SKIP_TAG (~'/')* '/>')) -> skip;

//INT
ATTR_VALUE_INT : (ATTR_DQ ('-')?(DIGIT)+ ATTR_DQ | ATTR_QU ('-')?(DIGIT)+ ATTR_QU);

//REAL
ATTR_VALUE_REAL : ((ATTR_DQ '-'? DIGIT+ '.' DIGIT+ ATTR_DQ) | (ATTR_QU '-'? DIGIT+ '.' DIGIT+ ATTR_QU));

//STR
ATTR_VALUE_STR : (ATTR_DQ (~'"')* ATTR_DQ | ATTR_QU (~'\'')* ATTR_QU);

NAMECHAR : LETTER | DIGIT | '.' | '-' | '_' | ':';
DIGIT : [0-9] ;
LETTER : [a-z] | [A-Z] ;
WS: [ \t\r\n]+ -> skip ;
