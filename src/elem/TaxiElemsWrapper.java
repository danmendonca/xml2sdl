/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package elem;

/**
 *
 * @author Daniel
 */
public abstract class TaxiElemsWrapper {
    
    public abstract void print();
    public abstract String getLat();
    public abstract String getLon();
    public abstract String getHeading();
    
}
