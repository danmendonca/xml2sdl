package elem;

public class Tower {

    private double lat;
    private double lon;

    private String alt;

    public Tower() {
        lat = Double.NaN;
        lon = Double.NaN;
    }

    /**
     *
     */
    public void print() {

        System.out.println("### Tower - Attributes ###");

        if (!Double.isNaN(lat)) {
            System.out.println("Lat = " + this.lat);
        }

        if (!Double.isNaN(lon)) {
            System.out.println("Lon = " + this.lon);
        }

        if ((this.alt != null) && !this.alt.isEmpty()) {
            System.out.println("Alt = " + this.alt);
        }

        System.out.print(System.lineSeparator());
    }

    /**
     *
     * @param input
     */
    public void setLat(String input) throws NumberFormatException, IllegalArgumentException {
        if (Utility.checkDuplicDouble(lat, "Lat")) {
            String latStr = input.replaceAll("^\"|\"$", "");
            this.lat = Double.parseDouble(latStr);

            if ((this.lat < -90) || (this.lat > 90)) {
                this.lat = Double.NaN;
                throw new IllegalArgumentException("Lat: -90 to +90 degrees");
            }
        }
    }

    /**
     *
     * @param input
     */
    public void setLon(String input) throws NumberFormatException, IllegalArgumentException {
        if (Utility.checkDuplicDouble(lon, "Lon")) {
            String lonStr = input.replaceAll("^\"|\"$", "");
            this.lon = Double.parseDouble(lonStr);

            if ((this.lon < -180) || (this.lon > 180)) {
                this.lon = Double.NaN;
                throw new IllegalArgumentException("Lon: -180 to +180 degrees");
            }
        }
    }

    /**
     *
     * @param input
     */
    public void setAlt(String input) {
        if(Utility.checkDuplicString(alt, "Alt")){
            alt = Utility.setMeterFeet(input, "Alt");
        }
    }

}
