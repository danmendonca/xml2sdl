package elem;

public class GlideSlope {

    private double lat;
    private double lon;
    private double pitch;

    private String alt;
    private String range;

    public GlideSlope() {
        lat = Double.NaN;
        lon = Double.NaN;
        pitch = Double.NaN;
    }

    /**
     *
     */
    public void print() {

        System.out.println("### GlideSlope- Attributes ###");

        if (!Double.isNaN(lat)) {
            System.out.println("Lat = " + this.lat);
        }

        if (!Double.isNaN(lon)) {
            System.out.println("Lon = " + this.lon);
        }

        if ((this.alt != null) && !this.alt.isEmpty()) {
            System.out.println("Alt = " + this.alt);
        }

        if (!Double.isNaN(pitch)) {
            System.out.println("Pitch =" + this.pitch);
        }

        if ((this.range != null) && !this.range.isEmpty()) {
            System.out.println("Range= " + this.range);
        }

        System.out.print(System.lineSeparator());
    }

    /**
     *
     * @throws IllegalArgumentException
     */
    public void checkMissingAttr() throws IllegalArgumentException {
        Utility.checkMandatoryDouble("Lat", lat);
        Utility.checkMandatoryDouble("Lon", lon);
        Utility.checkMandatoryString("Alt", alt);
        Utility.checkMandatoryDouble("Pitch", pitch);
        Utility.checkMandatoryString("Range", range);
    }

    /**
     *
     * @param input
     */
    public void setLat(String input) throws NumberFormatException, IllegalArgumentException {
        if (Utility.checkDuplicDouble(lat, "Lat")) {
            String latStr = input.replaceAll("^\"|\"$", "");
            this.lat = Double.parseDouble(latStr);

            if ((this.lat < -90) || (this.lat > 90)) {
                this.lat = Double.NaN;
                throw new IllegalArgumentException("Lat: -90 to +90 degrees");
            }
        }
    }

    /**
     *
     * @param input
     */
    public void setLon(String input) throws NumberFormatException, IllegalArgumentException {
        if (Utility.checkDuplicDouble(lon, "Lon")) {
            String lonStr = input.replaceAll("^\"|\"$", "");
            this.lon = Double.parseDouble(lonStr);

            if ((this.lon < -180) || (this.lon > 180)) {
                this.lon = Double.NaN;
                throw new IllegalArgumentException("Lon: -180 to +180 degrees");
            }
        }
    }

    /**
     *
     * @param input
     */
    public void setAlt(String input) {
        if (Utility.checkDuplicString(alt, "Alt")) {
            alt = Utility.setMeterFeet(input, "Alt");
        }
    }

    /**
     *
     * @param input
     * @throws NumberFormatException
     * @throws IllegalArgumentException
     */
    public void setPitch(String input) throws NumberFormatException, IllegalArgumentException {
        if (Utility.checkDuplicDouble(pitch, "Pitch")) {
            String pitchStr = input.replaceAll("^\"|\"$", "");
            this.pitch = Double.parseDouble(pitchStr);

            if ((this.pitch < 0.0) || (this.pitch > 360.0)) {
                this.pitch = Double.NaN;
                throw new IllegalArgumentException("Pitch: 0 to 360 ");
            }
        }
    }

    /**
     *
     * @param input
     * @throws NumberFormatException
     * @throws IllegalArgumentException
     *
     */
    /* *
     * @param input
     */
    public void setRange(String input) {
        if (Utility.checkDuplicString(range, "Range")) {
            range = Utility.setNauticalMeterFeet(input, "Range");
        }
    }
}
