package elem;



public class Markings {
    private boolean edges;
    private boolean threshold;
    private boolean fixedDistance;
    private boolean touchdown;
    private boolean dashes;
    private boolean ident;
    private boolean precision;
    private boolean edgePavement;
    private boolean singleEnd;
    private boolean primaryClosed;
    private boolean secondaryClosed;
    private boolean primaryStol;
    private boolean secondaryStol;
    private boolean alternateThreshold;
    private boolean alternateTouchdown;
    private boolean alternateFixedDistance;
    private boolean alternatePrecision;
    private boolean leadingZeroIdent;
    private boolean noThresholdEndArrows;

    private boolean edgesSet;
    private boolean thresholdSet;
    private boolean fixedDistanceSet;
    private boolean touchdownSet;
    private boolean dashesSet;
    private boolean identSet;
    private boolean precisionSet;
    private boolean edgePavementSet;
    private boolean singleEndSet;
    private boolean primaryClosedSet;
    private boolean secondaryClosedSet;
    private boolean primaryStolSet;
    private boolean secondaryStolSet;
    private boolean alternateThresholdSet;
    private boolean alternateTouchdownSet;
    private boolean alternateFixedDistanceSet;
    private boolean alternatePrecisionSet;
    private boolean leadingZeroIdentSet;
    private boolean noThresholdEndArrowsSet;



    public Markings() {
        edges = false;
        threshold = false;
        fixedDistance = false;
        touchdown = false;
        dashes = false;
        ident = false;
        precision = false;
        edgePavement = false;
        singleEnd = false;
        primaryClosed = false;
        secondaryClosed = false;
        primaryStol = false;
        secondaryStol = false;
        alternateThreshold = false;
        alternateTouchdown = false;
        alternateFixedDistance = false;
        alternatePrecision = false;
        leadingZeroIdent = false;
        noThresholdEndArrows = false;


        edgesSet = false;
        thresholdSet = false;
        fixedDistanceSet = false;
        touchdownSet = false;
        dashesSet = false;
        identSet = false;
        precisionSet = false;
        edgePavementSet = false;
        singleEndSet = false;
        primaryClosedSet = false;
        secondaryClosedSet = false;
        primaryStolSet = false;
        secondaryStolSet = false;
        alternateThresholdSet = false;
        alternateTouchdownSet = false;
        alternateFixedDistanceSet = false;
        alternatePrecisionSet = false;
        leadingZeroIdentSet = false;
        noThresholdEndArrowsSet = false;
    }


    public void print() {
        System.out.println("### Markings - Attributes ###");

        if(edgesSet)
            System.out.println("Edges = " + edges);

        if(thresholdSet)
            System.out.println("Threshold = " + threshold);

        if(fixedDistanceSet)
            System.out.println("Fixed Distance = " + fixedDistance);

        if(touchdownSet)
            System.out.println("TouchDown = " + touchdown);

        if(dashesSet)
            System.out.println("Dashes = " + dashes);

        if(identSet)
            System.out.println("Ident = " + ident);

        if(precisionSet)
            System.out.println("Precision = " + precision);

        if(edgePavementSet)
            System.out.println("Edge Pavement = " + edgePavement);

        if(singleEndSet)
            System.out.println("Single End = " + singleEnd);

        if(primaryClosedSet)
            System.out.println("Primary Closed = " + primaryClosed);

        if(secondaryClosedSet)
            System.out.println("Secondary Closed = " + secondaryClosed);

        if(primaryStolSet)
            System.out.println("Primary Stol = " + primaryStol);

        if(secondaryStolSet)
            System.out.println("Secondary Stol = " + secondaryStol);
        
        if(alternateThresholdSet)
            System.out.println("alternateThreshold = " + alternateThreshold);
        
        if(alternateTouchdownSet)
            System.out.println("alternateTouchdown = " + alternateTouchdown);
        
        if(alternateFixedDistanceSet)
            System.out.println("alternateFixedDistance = " + alternateFixedDistance);
        
        if(alternatePrecisionSet)
            System.out.println("alternatePrecision = " + alternatePrecision);
        
        if(leadingZeroIdentSet)
            System.out.println("leadingZeroIdent = " + leadingZeroIdent);
        
        if(noThresholdEndArrowsSet)
            System.out.println("noThresholdEndArrows = " + noThresholdEndArrows);
        
        System.out.print(System.lineSeparator());
    }


    public void setAlternateThreshold(String input) throws IllegalArgumentException {
        
        if(alternateThresholdSet) throw new IllegalArgumentException("AlternateThreshold - Duplicated Attribute");
        
        String inputStr = input.replaceAll("^\"|\"$", "");
        if(inputStr.equals("TRUE") || inputStr.equals("FALSE")) {
            this.alternateThreshold = Boolean.parseBoolean(inputStr);
            this.alternateThresholdSet = true;
        } else
            throw new IllegalArgumentException("Alternate Threshold: Valid value TRUE or FALSE.");
    }


    public void setAlternateTouchdown(String input) throws IllegalArgumentException {
        
        if(alternateTouchdownSet) throw new IllegalArgumentException("AlternateTouchDown - Duplicated Attribute");
        
        String inputStr = input.replaceAll("^\"|\"$", "");
        if(inputStr.equals("TRUE") || inputStr.equals("FALSE")) {
            this.alternateTouchdown = Boolean.parseBoolean(inputStr);
            this.alternateTouchdownSet = true;
        } else
            throw new IllegalArgumentException("Alternate Touchdown: Valid value TRUE or FALSE.");
    }


    public void setAlternateFixedDistance(String input) throws IllegalArgumentException {
        
        if(alternateFixedDistanceSet) throw new IllegalArgumentException("AlternateFixedDistance - Duplicated Attribute");
        
        String inputStr = input.replaceAll("^\"|\"$", "");
        if(inputStr.equals("TRUE") || inputStr.equals("FALSE")) {
            this.alternateFixedDistance = Boolean.parseBoolean(inputStr);
            this.alternateFixedDistanceSet = true;
        } else
            throw new IllegalArgumentException("Alternate Fixed Distance: Valid value TRUE or FALSE.");
    }


    public void setAlternatePrecision(String input) throws IllegalArgumentException {
        
        if(alternatePrecisionSet) throw new IllegalArgumentException("AlternatePrecision - Duplicated Attribute");
        
        String inputStr = input.replaceAll("^\"|\"$", "");
        if(inputStr.equals("TRUE") || inputStr.equals("FALSE")) {
            this.alternatePrecision = Boolean.parseBoolean(inputStr);
            this.alternatePrecisionSet = true;
        } else
            throw new IllegalArgumentException("Alternate Precision: Valid value TRUE or FALSE.");
    }


    public void setLeadingZeroIdent(String input) throws IllegalArgumentException {
        
        if(leadingZeroIdentSet) throw new IllegalArgumentException("LeadingZeroIdent - Duplicated Attribute");
        
        String inputStr = input.replaceAll("^\"|\"$", "");
        if(inputStr.equals("TRUE") || inputStr.equals("FALSE")) {
            this.leadingZeroIdent = Boolean.parseBoolean(inputStr);
            this.leadingZeroIdentSet = true;
        } else
            throw new IllegalArgumentException("Leading Zero Ident: Valid value TRUE or FALSE.");
    }


    public void setNoThresholdEndArrows(String input) throws IllegalArgumentException {
        
        if(noThresholdEndArrowsSet) throw new IllegalArgumentException("NoThresholdEndArrows - Duplicated Attribute");
        
        String inputStr = input.replaceAll("^\"|\"$", "");
        if(inputStr.equals("TRUE") || inputStr.equals("FALSE")) {
            this.noThresholdEndArrows = Boolean.parseBoolean(inputStr);
            this.noThresholdEndArrowsSet = true;
        } else
            throw new IllegalArgumentException("No Threshold End Arrows: Valid value TRUE or FALSE.");
    }


    /**
     *
     * @param input
     */
    public void setEdges(String input) throws IllegalArgumentException {
        
        if(edgesSet) throw new IllegalArgumentException("Edges - Duplicated Attribute");
        
        String inputStr = input.replaceAll("^\"|\"$", "");
        if(inputStr.equals("TRUE") || inputStr.equals("FALSE")) {
            this.edges = Boolean.parseBoolean(inputStr);
            this.edgesSet = true;
        } else
            throw new IllegalArgumentException("Edges: Valid value TRUE or FALSE.");
    }


    /**
     *
     * @param input
     */
    public void setThreshold(String input) throws IllegalArgumentException {
        
        if(thresholdSet) throw new IllegalArgumentException("Threshold - Duplicated Attribute");
        
        String inputStr = input.replaceAll("^\"|\"$", "");
        if(inputStr.equals("TRUE") || inputStr.equals("FALSE")) {
            this.threshold = Boolean.parseBoolean(inputStr);
            this.thresholdSet = true;
        } else
            throw new IllegalArgumentException("Threshold: Valid value TRUE or FALSE.");
    }


    /**
     *
     * @param input
     */
    public void setFixedDistance(String input) throws IllegalArgumentException {
        
        if(fixedDistanceSet) throw new IllegalArgumentException("FixedDistance - Duplicated Attribute");
        
        String inputStr = input.replaceAll("^\"|\"$", "");
        if(inputStr.equals("TRUE") || inputStr.equals("FALSE")) {
            this.fixedDistance = Boolean.parseBoolean(inputStr);
            this.fixedDistanceSet = true;
        } else
            throw new IllegalArgumentException("Fixed Distance: Valid value TRUE or FALSE.");
    }


    /**
     *
     * @param input
     */
    public void setTouchdown(String input) throws IllegalArgumentException {
        
        if(touchdownSet) throw new IllegalArgumentException("Touchdown - Duplicated Attribute");
        
        String inputStr = input.replaceAll("^\"|\"$", "");
        if(inputStr.equals("TRUE") || inputStr.equals("FALSE")) {
            this.touchdown = Boolean.parseBoolean(inputStr);
            this.touchdownSet = true;
        } else
            throw new IllegalArgumentException("Touchdown: Valid value TRUE or FALSE.");
    }


    /**
     *
     * @param input
     */
    public void setDashes(String input) throws IllegalArgumentException {
        
        if(dashesSet) throw new IllegalArgumentException("Dashes - Duplicated Attribute");
        
        String inputStr = input.replaceAll("^\"|\"$", "");
        if(inputStr.equals("TRUE") || inputStr.equals("FALSE")) {
            this.dashes = Boolean.parseBoolean(inputStr);
            this.dashesSet = true;
        } else
            throw new IllegalArgumentException("Dashes: Valid value TRUE or FALSE.");
    }


    /**
     *
     * @param input
     */
    public void setIdent(String input) throws IllegalArgumentException {
        
        if(identSet) throw new IllegalArgumentException("Ident - Duplicated Attribute");
        
        String inputStr = input.replaceAll("^\"|\"$", "");
        if(inputStr.equals("TRUE") || inputStr.equals("FALSE")) {
            this.ident = Boolean.parseBoolean(inputStr);
            this.identSet = true;
        } else
            throw new IllegalArgumentException("Ident: Valid value TRUE or FALSE.");
    }


    /**
     *
     * @param input
     */
    public void setPrecision(String input) throws IllegalArgumentException {
        
        if(precisionSet) throw new IllegalArgumentException("Precision - Duplicated Attribute");
        
        String inputStr = input.replaceAll("^\"|\"$", "");
        if(inputStr.equals("TRUE") || inputStr.equals("FALSE")) {
            this.precision = Boolean.parseBoolean(inputStr);
            this.precisionSet = true;
        } else
            throw new IllegalArgumentException("Precision: Valid value TRUE or FALSE.");
    }


    /**
     *
     * @param input
     */
    public void setEdgePavement(String input) throws IllegalArgumentException {
        
        if(edgePavementSet) throw new IllegalArgumentException("EdgePavement - Duplicated Attribute");
        
        String inputStr = input.replaceAll("^\"|\"$", "");
        if(inputStr.equals("TRUE") || inputStr.equals("FALSE")) {
            this.edgePavement = Boolean.parseBoolean(inputStr);
            this.edgePavementSet = true;
        } else
            throw new IllegalArgumentException("Edge Pavement: Valid value TRUE or FALSE.");
    }


    /**
     *
     * @param input
     */
    public void setSingleEnd(String input) throws IllegalArgumentException {
        
        if(singleEndSet) throw new IllegalArgumentException("SingleEnd - Duplicated Attribute");
        
        String inputStr = input.replaceAll("^\"|\"$", "");
        if(inputStr.equals("TRUE") || inputStr.equals("FALSE")) {
            this.singleEnd = Boolean.parseBoolean(inputStr);
            this.singleEndSet = true;
        } else
            throw new IllegalArgumentException("Sngle End: Valid value TRUE or FALSE.");
    }


    /**
     *
     * @param input
     */
    public void setPrimaryClosed(String input) throws IllegalArgumentException {
        
        if(primaryClosedSet) throw new IllegalArgumentException("PrimaryClosed - Duplicated Attribute");
        
        String inputStr = input.replaceAll("^\"|\"$", "");
        if(inputStr.equals("TRUE") || inputStr.equals("FALSE")) {
            this.primaryClosed = Boolean.parseBoolean(inputStr);
            this.primaryClosedSet = true;
        } else
            throw new IllegalArgumentException("Primary Closed: Valid value TRUE or FALSE.");
    }


    /**
     *
     * @param input
     */
    public void setSecondaryClosed(String input) throws IllegalArgumentException {
        
        if(secondaryClosedSet) throw new IllegalArgumentException("SecondaryClosed - Duplicated Attribute");
        
        String inputStr = input.replaceAll("^\"|\"$", "");
        if(inputStr.equals("TRUE") || inputStr.equals("FALSE")) {
            this.secondaryClosed = Boolean.parseBoolean(inputStr);
            this.secondaryClosedSet = true;
        } else
            throw new IllegalArgumentException("Secondary Closed: Valid value TRUE or FALSE.");
    }


    /**
     *
     * @param input
     */
    public void setPrimaryStol(String input) throws IllegalArgumentException {
        
        if(primaryStolSet) throw new IllegalArgumentException("PrimaryStol - Duplicated Attribute");
        
        String inputStr = input.replaceAll("^\"|\"$", "");
        if(inputStr.equals("TRUE") || inputStr.equals("FALSE")) {
            this.primaryStol = Boolean.parseBoolean(inputStr);
            this.primaryStolSet = true;
        } else
            throw new IllegalArgumentException("Primary Stol: Valid value TRUE or FALSE.");
    }


    /**
     *
     * @param input
     */
    public void setSecondaryStol(String input) throws IllegalArgumentException {
        
        if(secondaryStolSet) throw new IllegalArgumentException("SecondaryStol - Duplicated Attribute");
        
        String inputStr = input.replaceAll("^\"|\"$", "");
        if(inputStr.equals("TRUE") || inputStr.equals("FALSE")) {
            this.secondaryStol = Boolean.parseBoolean(inputStr);
            this.secondaryStolSet = true;
        } else
            throw new IllegalArgumentException("Secondary Stol: Valid value TRUE or FALSE.");
    }
}
