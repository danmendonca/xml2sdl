package elem;


import java.util.HashSet;

public class VisualModel {
    private double heading;
    private String imageComplexity;
    private String name;
    private String instanceId;


    private boolean headingSet;
    private boolean imageComplexitySet;
    private boolean nameSet;
    private boolean instanceIdSet;

    private final static HashSet<String> imageComplexityValues;

    static {
        imageComplexityValues = new HashSet<>();
        imageComplexityValues.add("VERY_SPARSE");
        imageComplexityValues.add("SPARSE");
        imageComplexityValues.add("NORMAL");
        imageComplexityValues.add("DENSE");
        imageComplexityValues.add("VERY_DENSE");
    }


    public VisualModel() {
        headingSet = false;
        imageComplexitySet = false;
        nameSet = false;
        instanceIdSet = false;

        heading = Double.NaN;
    }


    public void print() {
        System.out.println("### VisualModel - Attributes ###");

        if(headingSet)
            System.out.println("Heading = " + heading);

        if(imageComplexitySet)
            System.out.println("ImageComplexity = " + imageComplexity);

        if(nameSet)
            System.out.println("Name = " + name);

        if(instanceIdSet)
            System.out.println("Instance ID = " + instanceId);
        
        System.out.print(System.lineSeparator());
    }


    public void setHeading(String input) throws IllegalArgumentException {
        String inputStr = input.replaceAll("^\"|\"$", "");
        double tmp = Double.parseDouble(inputStr);

        if(tmp >= 0.0 && tmp <= 360.0) {
            headingSet = true;
            heading = tmp;
        } else
            throw new IllegalArgumentException("Heading: Invalid value. It must be between 0.0 and 360.0");
    }


    public void setImageComplexity(String input) throws IllegalArgumentException {
        String inputStr = input.replaceAll("^\"|\"$", "");

        if(imageComplexityValues.contains(inputStr)) {
            imageComplexitySet = true;
            imageComplexity = inputStr;
        } else
            throw new IllegalArgumentException("Image Complexity: Invalid value.");
    }


    public void setName(String input) throws IllegalArgumentException {
        String inputStr = input.replaceAll("^\"|\"$", "");

        if(inputStr.matches("[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}")) {
            nameSet = true;
            name = inputStr;
        } else
            throw new IllegalArgumentException("Name: Invalid format. Format must be: {nnnnnnnn-nnnn-nnnn-nnnn-nnnnnnnnnnnn}");
    }


    public void setInstanceId(String input) throws IllegalArgumentException {
        String inputStr = input.replaceAll("^\"|\"$", "");

        if(inputStr.matches("[a-f0-9]{8}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{4}-[a-f0-9]{12}")) {
            instanceIdSet = true;
            instanceId = inputStr;
        } else
            throw new IllegalArgumentException("Instance ID: Invalid format. Format must be: {nnnnnnnn-nnnn-nnnn-nnnn-nnnnnnnnnnnn}");
    }
}
