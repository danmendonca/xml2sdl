package elem;

import java.util.HashSet;

public class Overrun {

    private String end;
    private String length;
    private String width;
    private String surface;

    public boolean endSet;
    private boolean lengthSet;
    private boolean widthSet;
    private boolean surfaceSet;

    private final static HashSet<String> surfaceValuesSet;

    static {
        surfaceValuesSet = new HashSet<>();
        surfaceValuesSet.add("ASPHALT");
        surfaceValuesSet.add("BITUMINOUS");
        surfaceValuesSet.add("BRICK");
        surfaceValuesSet.add("CLAY");
        surfaceValuesSet.add("CEMENT");
        surfaceValuesSet.add("CONCRETE");
        surfaceValuesSet.add("CORAL");
        surfaceValuesSet.add("DIRT");
        surfaceValuesSet.add("GRASS");
        surfaceValuesSet.add("GRAVEL");
        surfaceValuesSet.add("ICE");
        surfaceValuesSet.add("MACADAM");
        surfaceValuesSet.add("OIL_TREATED, PLANKS");
        surfaceValuesSet.add("SAND");
        surfaceValuesSet.add("SHALE");
        surfaceValuesSet.add("SNOW");
        surfaceValuesSet.add("STEEL_MATS");
        surfaceValuesSet.add("TARMAC");
        surfaceValuesSet.add("UNKNOWN");
        surfaceValuesSet.add("WATER");
    }

    public Overrun() {
        endSet = false;
        lengthSet = false;
        widthSet = false;
        surfaceSet = false;
    }

    public void print() {
        System.out.println("### Overrun - Attributes ###");

        if (endSet) {
            System.out.println("End = " + end);
        }

        if (lengthSet) {
            System.out.println("Length = " + length);
        }

        if (widthSet) {
            System.out.println("Width = " + width);
        }

        if (surfaceSet) {
            System.out.println("Surface = " + surface);
        }

        System.out.print(System.lineSeparator());
    }

    /**
     *
     * @throws IllegalArgumentException
     */
    public void checkMissingAttr() throws IllegalArgumentException {
        Utility.checkMandatoryString("End", end);
        Utility.checkMandatoryString("Length", length);
    }

    /**
     *
     * @param input
     * @throws IllegalArgumentException
     */
    public void setEnd(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(end, "End")) {
            String inputStr = input.replaceAll("^\"|\"$", "");
            if (inputStr.equals("PRIMARY") || inputStr.equals("SECONDARY")) {
                endSet = true;
                end = inputStr;
            } else {
                throw new IllegalArgumentException("End: Invalid value. Supported values are PRIMARY and SECONDARY.");
            }
        }
    }

    /**
     *
     * @param input
     */
    public void setLength(String input) {
        if (Utility.checkDuplicString(length, "Length")) {
            length = Utility.setMeterFeet(input, "Length");
            lengthSet = true;
        }
    }

    /**
     *
     * @param input
     */
    public void setWidth(String input) {
        if (Utility.checkDuplicString(width, "Width")) {
            width = Utility.setMeterFeet(input, "Width");
            widthSet = true;
        }
    }

    /**
     *
     * @param input
     * @throws IllegalArgumentException
     */
    public void setSurface(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(surface, "Surface")) {
            String inputStr = input.replaceAll("^\"|\"$", "");
            if (surfaceValuesSet.contains(inputStr)) {
                surface = inputStr;
                surfaceSet = true;
            } else {
                throw new IllegalArgumentException("Surface: Invalid value.");
            }
        }
    }
}
