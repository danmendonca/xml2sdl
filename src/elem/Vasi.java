package elem;

import java.util.HashSet;

public class Vasi {

    private String end;
    private String type;
    private String side;
    private String biasX;
    private String biasZ;
    private String spacing;
    private double pitch;

    private boolean endSet;
    private boolean typeSet;
    private boolean sideSet;
    private boolean biasXSet;
    private boolean biasZSet;
    private boolean spacingSet;
    private boolean pitchSet;

    private final static HashSet<String> typeValues;

    static {
        typeValues = new HashSet<>();
        typeValues.add("PAPI2");
        typeValues.add("PAPI4");
        typeValues.add("PVASI");
        typeValues.add("TRICOLOR");
        typeValues.add("TVASI");
        typeValues.add("VASI21");
        typeValues.add("VASI22");
        typeValues.add("VASI23");
        typeValues.add("VASI31");
        typeValues.add("VASI32");
        typeValues.add("VASI33");
        typeValues.add("BALL");
        typeValues.add("APAP");
        typeValues.add("PANELS");
    }

    public Vasi() {
        endSet = false;
        typeSet = false;
        sideSet = false;
        biasXSet = false;
        biasZSet = false;
        spacingSet = false;
        pitchSet = false;

        pitch = Double.NaN;
    }

    public void print() {
        System.out.println("### Vasi - Attributes ###");

        if (endSet) {
            System.out.println("End = " + end);
        }

        if (typeSet) {
            System.out.println("Type = " + type);
        }

        if (sideSet) {
            System.out.println("Side = " + side);
        }

        if (biasXSet) {
            System.out.println("BiasX = " + biasX);
        }

        if (biasZSet) {
            System.out.println("BiasZ = " + biasZ);
        }

        if (spacingSet) {
            System.out.println("Spacing = " + spacing);
        }

        if (pitchSet) {
            System.out.println("Pitch = " + pitch);
        }

        System.out.print(System.lineSeparator());
    }

    /**
     *
     * @throws IllegalArgumentException
     */
    public void checkMissingAttr() throws IllegalArgumentException {
        Utility.checkMandatoryString("End", end);
        Utility.checkMandatoryString("Type", type);
        Utility.checkMandatoryString("Side", side);
        Utility.checkMandatoryString("BiasX", biasX);
        Utility.checkMandatoryString("BiasZ", biasZ);
        Utility.checkMandatoryString("Spacing", spacing);
        Utility.checkMandatoryDouble("Pitch", pitch);
    }

    public void setEnd(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(end, "End")) {
            String inputStr = input.replaceAll("^\"|\"$", "");
            if (inputStr.equals("PRIMARY") || inputStr.equals("SECONDARY")) {
                endSet = true;
                end = inputStr;
            } else {
                throw new IllegalArgumentException("End: Invalid value. Supported values are PRIMARY and SECONDARY.");
            }
        }
    }

    public void setType(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(type, "Type")) {
            String inputStr = input.replaceAll("^\"|\"$", "");

            if (typeValues.contains(inputStr)) {
                typeSet = true;
                type = inputStr;
            } else {
                throw new IllegalArgumentException("Type: Invalid type.");
            }
        }
    }

    public void setSide(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(side, "Side")) {
            String inputStr = input.replaceAll("^\"|\"$", "");
            if (inputStr.equals("LEFT") || inputStr.equals("RIGHT")) {
                sideSet = true;
                side = inputStr;
            } else {
                throw new IllegalArgumentException("Side: Invalid value. Supported values are RIGHT and LEFT.");
            }
        }
    }

    public void setBiasX(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(biasX, "Bias X")) {
            String inputStr = input.replaceAll("^\"|\"$", "");

            if (!inputStr.isEmpty()) {
                biasXSet = true;
                biasX = inputStr;
            } else {
                throw new IllegalArgumentException("BiasX: Invalid value.");
            }
        }
    }

    public void setBiasZ(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(biasZ, "Bias Z")) {
            String inputStr = input.replaceAll("^\"|\"$", "");

            if (!inputStr.isEmpty()) {
                biasZSet = true;
                biasZ = inputStr;
            } else {
                throw new IllegalArgumentException("BiasZ: Invalid value.");
            }
        }
    }

    public void setSpacing(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(spacing, "Spacing")) {
            String inputStr = input.replaceAll("^\"|\"$", "");

            if (!inputStr.isEmpty()) {
                spacingSet = true;
                spacing = inputStr;
            } else {
                throw new IllegalArgumentException("S+acing: Invalid value.");
            }
        }
    }

    public void setPitch(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicDouble(pitch, "Pitch")) {
            String inputStr = input.replaceAll("^\"|\"$", "");
            double tmp = Double.parseDouble(inputStr);

            if (tmp >= 0.0 && tmp <= 10.0) {
                pitchSet = true;
                pitch = tmp;
            } else {
                throw new IllegalArgumentException("Pitch: Invalid value.");
            }
        }
    }
}
