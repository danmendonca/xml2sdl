package elem;

public class TaxiwayPoint {

    private int index;

    private double biasX;
    private double biasZ;
    private double lon;
    private double lat;

    private String type;
    private String orientation;

    public TaxiwayPoint() {
        biasX = Double.NaN;
        biasZ = Double.NaN;
        lon = Double.NaN;
        lat = Double.NaN;
        index = Integer.MAX_VALUE;
    }

    public int getIndex() {
        return index;
    }

    /**
     *
     */
    public void print() {

        System.out.println("### TaxiwayPoint - Attributes ###");
        Utility.printInt("Index", index);
        Utility.printString("Type", type);
        Utility.printString("Orientation", orientation);
        Utility.printDouble("Lat", lat);
        Utility.printDouble("Lon", lon);
        Utility.printDouble("Bias X", biasX);
        Utility.printDouble("Bias Z", biasZ);
        System.out.print(System.lineSeparator());
    }

    /**
     *
     * @throws IllegalArgumentException
     */
    public void checkMissingAttr() throws IllegalArgumentException {
        Utility.checkMandatoryInt("Index", index);
        Utility.checkMandatoryString("Type", type);
        //Utility.checkMandatoryString("Orientation", orientation);

        if ((Double.isNaN(lat) || Double.isNaN(lon)) && (Double.isNaN(biasX) || Double.isNaN(biasZ))) {
            throw new IllegalArgumentException("(Lat-Lon) or (BiasX-BiasZ) - Missing mandatory attribute");
        }

    }

    /**
     *
     * @param input
     */
    public void setIndex(String input) throws NumberFormatException, IllegalArgumentException {
        if (Utility.checkDuplicInt(index, "Index")) {
            index = Utility.setInt(input, "Index", 0, 3999);
        }
    }

    /**
     *
     * @param input
     */
    public void setType(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(type, "Type")) {
            String typeStr = input.replaceAll("^\"|\"$", "");

            switch (typeStr) {
                case "NORMAL":
                    this.type = "NORMAL";
                    break;
                case "HOLD_SHORT":
                    this.type = "HOLD_SHORT";
                    break;
                case "ILS_HOLD_SHORT":
                    this.type = "ILS_HOLD_SHORT";
                    break;
                case "HOLD_SHORT_NO_DRAW":
                    this.type = "HOLD_SHORT_NO_DRAW";
                    break;
                case "ILS_HOLD_SHORT_NO_DRAW":
                    this.type = "ILS_HOLD_SHORT_NO_DRAW";
                    break;
                default:
                    throw new IllegalArgumentException("Type: Invalid type");
            }
        }
    }

    /**
     *
     * @param input
     */
    public void setOrientation(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(orientation, "Orientation")) {
            String orientationStr = input.replaceAll("^\"|\"$", "");

            switch (orientationStr) {
                case "FORWARD":
                    this.orientation = "FORWARD";
                    break;
                case "REVERSE":
                    this.orientation = "REVERSE";
                    break;
                default:
                    throw new IllegalArgumentException("Orientation: Invalid orientation");
            }
        }
    }

    /**
     *
     * @param input
     */
    public void setLat(String input) throws NumberFormatException, IllegalArgumentException {
        if (!Double.isNaN(biasX) || !Double.isNaN(biasZ)) {
            throw new IllegalArgumentException("Can either have a lat-lon position, or a bias.");
        }
        if (Utility.checkDuplicDouble(lat, "Lat")) {
            lat = Utility.setDouble(input, "Lat", -90.0, 90.0);
        }
    }

    /**
     *
     * @param input
     */
    public void setLon(String input) throws NumberFormatException, IllegalArgumentException {
        if (!Double.isNaN(biasX) || !Double.isNaN(biasZ)) {
            throw new IllegalArgumentException("Can either have a lat-lon position, or a bias.");
        }
        if (Utility.checkDuplicDouble(lon, "Lon")) {
            lon = Utility.setDouble(input, "Lon", -180.0, 180.0);
        }
    }

    /**
     *
     * @param input
     */
    public void setBiasX(String input) throws NumberFormatException, IllegalArgumentException {
        if (!Double.isNaN(lat) || !Double.isNaN(lon)) {
            throw new IllegalArgumentException("Can either have a lat-lon position, or a bias");
        }
        if (Utility.checkDuplicDouble(biasX, "Bias X")) {
            biasX = Utility.setDouble(input, "Bias X", Double.MIN_VALUE, Double.MAX_VALUE);
        }
    }

    /**
     *
     * @param input
     */
    public void setBiasZ(String input) throws NumberFormatException, IllegalArgumentException {
        if (!Double.isNaN(lat) || !Double.isNaN(lon)) {
            throw new IllegalArgumentException("Can either have a lat-lon position, or a bias");
        }
        if (Utility.checkDuplicDouble(biasZ, "Bias Z")) {
            biasZ = Utility.setDouble(input, "Bias Z", Double.MIN_VALUE, Double.MAX_VALUE);
        }
    }
}
