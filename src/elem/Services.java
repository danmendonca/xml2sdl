package elem;

import java.util.ArrayList;

public class Services {
    
    private ArrayList<Fuel> fuelList;
    
    public Services(){
        fuelList = new ArrayList<>();
    }
    
    public void addFuel(Fuel fuelElem){
        fuelList.add(fuelElem);
    }
    
    public void print(){
        
        System.out.println("### Services - Attributes ###");
        
        for(Fuel fElem : fuelList){
            fElem.print();
        }
    }
}
