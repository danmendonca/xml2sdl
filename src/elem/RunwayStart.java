package elem;

public class RunwayStart {

    private double lat;
    private double lon;
    private double heading;

    private String alt;
    private String type;
    private String end;

    public RunwayStart() {
        lat = Double.NaN;
        lon = Double.NaN;
        heading = Double.NaN;
    }

    /**
     *
     */
    public void print() {

        System.out.println("### RunwayStart - Attributes ###");

        if ((this.type != null) && !this.type.isEmpty()) {
            System.out.println("Type = " + this.type);
        }

        if (!Double.isNaN(lat)) {
            System.out.println("Lat = " + this.lat);
        }

        if (!Double.isNaN(lon)) {
            System.out.println("Lon = " + this.lon);
        }

        if ((this.alt != null) && !this.alt.isEmpty()) {
            System.out.println("Alt = " + this.alt);
        }
        if (!Double.isNaN(heading)) {
            System.out.println("Heading = " + this.heading);
        }

        if ((this.end != null) && !this.end.isEmpty()) {
            System.out.println("End= " + this.end);
        }

        System.out.print(System.lineSeparator());
    }

    /**
     *
     * @throws IllegalArgumentException
     */
    public void checkMissingAttr() throws IllegalArgumentException {
        Utility.checkMandatoryDouble("Lat", lat);
        Utility.checkMandatoryDouble("Lon", lon);
        Utility.checkMandatoryString("Alt", alt);
        Utility.checkMandatoryDouble("Heading", heading);
    }

    /**
     *
     * @param input
     * @throws IllegalArgumentException
     */
    public void setType(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(type, "Type")) {
            String typeStr = input.replaceAll("^\"|\"$", "");

            switch (typeStr) {
                case "RUNWAY":
                    this.type = "RUNWAY";
                    break;
                default:
                    throw new IllegalArgumentException("Type: Invalid type");
            }
        }
    }

    /**
     *
     * @param input
     */
    public void setLat(String input) throws NumberFormatException, IllegalArgumentException {
        if (Utility.checkDuplicDouble(lat, "Lat")) {
            String latStr = input.replaceAll("^\"|\"$", "");
            this.lat = Double.parseDouble(latStr);

            if ((this.lat < -90) || (this.lat > 90)) {
                this.lat = Double.NaN;
                throw new IllegalArgumentException("Lat: -90 to +90 degrees");
            }
        }
    }

    /**
     *
     * @param input
     */
    public void setLon(String input) throws NumberFormatException, IllegalArgumentException {
        if (Utility.checkDuplicDouble(lon, "Lon")) {
            String lonStr = input.replaceAll("^\"|\"$", "");
            this.lon = Double.parseDouble(lonStr);

            if ((this.lon < -180) || (this.lon > 180)) {
                this.lon = Double.NaN;
                throw new IllegalArgumentException("Lon: -180 to +180 degrees");
            }
        }
    }

    /**
     *
     * @param input
     */
    public void setAlt(String input) {
        if (Utility.checkDuplicString(alt, "Alt")) {
            alt = Utility.setMeterFeet(input, "Alt");
        }
    }

    /**
     *
     * @param input
     * @throws NumberFormatException
     * @throws IllegalArgumentException
     */
    public void setHeading(String input) throws NumberFormatException, IllegalArgumentException {
        if (Utility.checkDuplicDouble(heading, "Heading")) {
            String headingStr = input.replaceAll("^\"|\"$", "");
            heading = Double.parseDouble(headingStr);

            if ((heading < 0.0) || (heading > 360.0)) {
                heading = Double.NaN;
                throw new IllegalArgumentException("Heading: 0.0 to 360.0");
            }
        }
    }

    /**
     *
     * @param input
     * @throws IllegalArgumentException
     */
    public void setEnd(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(end, "End")) {
            String endStr = input.replaceAll("^\"|\"$", "");

            switch (endStr) {
                case "PRIMARY":
                    this.end = "PRIMARY";
                    break;
                case "SECONDARY":
                    this.end = "SECONDARY";
                default:
                    throw new IllegalArgumentException("End: Invalid end");
            }
        }
    }
}
