package elem;

import java.io.File;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;

public class Utility {

    //Generate SDL file
    public static void generateSDLfile(File outputFile, ArrayList<Airport> airportList) {

        

    }

    private Utility() {
        throw new AssertionError();
    }

    //Check duplicated attributes ==================================
    /**
     *
     * @param attr
     * @param nameAttr
     * @return
     * @throws IllegalArgumentException
     */
    static public boolean checkDuplicInt(int attr, String nameAttr) throws IllegalArgumentException {
        if (attr != Integer.MAX_VALUE) {
            throw new IllegalArgumentException(nameAttr + " - Duplicated Attribute");
        }
        return true;
    }

    /**
     *
     * @param attr
     * @param nameAttr
     * @return
     * @throws IllegalArgumentException
     */
    static public boolean checkDuplicDouble(double attr, String nameAttr) throws IllegalArgumentException {
        if (!Double.isNaN(attr)) {
            throw new IllegalArgumentException(nameAttr + " - Duplicated Attribute");
        }
        return true;
    }

    /**
     *
     * @param attr
     * @param nameAttr
     * @return
     * @throws IllegalArgumentException
     */
    static public boolean checkDuplicString(String attr, String nameAttr) throws IllegalArgumentException {
        if ((attr != null) && !attr.isEmpty()) {
            throw new IllegalArgumentException(nameAttr + " - Duplicated Attribute");
        }
        return true;
    }

    // Print methods =======================================
    /**
     *
     * @param n
     * @param i
     */
    static public void printInt(String n, int i) {
        if (i != Integer.MAX_VALUE) {
            System.out.println(n + " = " + i);
        }
    }

    /**
     *
     * @param n
     * @param d
     */
    static public void printDouble(String n, double d) {
        if (!Double.isNaN(d)) {
            System.out.println(n + " = " + d);
        }
    }

    /**
     *
     * @param n
     * @param v
     */
    static public void printString(String n, String v) {
        if ((v != null) && !v.isEmpty()) {
            System.out.println(n + " = " + v);
        }
    }

    //Check mandatory attributes ==================================
    /**
     *
     * @param n
     * @param i
     * @throws IllegalArgumentException
     */
    static public void checkMandatoryInt(String n, int i) throws IllegalArgumentException {
        if (i == Integer.MAX_VALUE) {
            throw new IllegalArgumentException(n + " - Missing mandatory attribute");
        }
    }

    /**
     *
     * @param n
     * @param d
     * @throws IllegalArgumentException
     */
    static public void checkMandatoryDouble(String n, double d) throws IllegalArgumentException {
        if (Double.isNaN(d)) {
            throw new IllegalArgumentException(n + " - Missing mandatory attribute");
        }
    }

    /**
     *
     * @param n
     * @param s
     * @throws IllegalArgumentException
     */
    static public void checkMandatoryString(String n, String s) throws IllegalArgumentException {
        if ((s == null) || s.isEmpty()) {
            throw new IllegalArgumentException(n + " - Missing mandatory attribute");
        }
    }

    // SET Generic Methods =====================================
    /**
     *
     * @param input
     * @param nameAttr
     * @param maxLength
     * @return
     * @throws IllegalArgumentException
     */
    static public String setString(String input, String nameAttr, int maxLength) throws IllegalArgumentException {
        String temp = input.replaceAll("^\"|\"$", "");

        if (temp.length() > maxLength) {
            throw new IllegalArgumentException(nameAttr + ": characters max = " + maxLength);
        }
        return temp;
    }

    /**
     *
     * @param input
     * @param nameAttr
     * @param minLim
     * @param maxLim
     * @return
     * @throws IllegalArgumentException
     * @throws NumberFormatException
     */
    static public double setDouble(String input, String nameAttr, double minLim, double maxLim) throws IllegalArgumentException, NumberFormatException {
        String temp = input.replaceAll("^\"|\"$", "");
        double tempDouble = Double.parseDouble(temp);

        if ((tempDouble < minLim) || (tempDouble > maxLim)) {
            throw new IllegalArgumentException(nameAttr + ": " + minLim + " to " + maxLim);
        }
        return tempDouble;
    }

    /**
     *
     * @param input
     * @param nameAttr
     * @param minLim
     * @param maxLim
     * @return
     * @throws IllegalArgumentException
     * @throws NumberFormatException
     */
    static public int setInt(String input, String nameAttr, int minLim, int maxLim) throws IllegalArgumentException, NumberFormatException {
        String temp = input.replaceAll("^\"|\"$", "");
        int tempInt = Integer.parseInt(temp);

        if ((tempInt < minLim) || (tempInt > maxLim)) {
            throw new IllegalArgumentException(nameAttr + ": " + minLim + " to " + maxLim);
        }
        return tempInt;
    }

    static public String setBoolean(String input, String nameAttr) throws IllegalArgumentException {
        String temp = input.replaceAll("^\"|\"$", "");

        if (temp.equals("TRUE") || temp.equals("FALSE")) {
            return temp;
        }

        throw new IllegalArgumentException(nameAttr + ": Must be TRUE or FALSE");
    }

    /**
     *
     * @param input
     * @param nameAttr
     * @return
     * @throws IllegalArgumentException
     * @throws NumberFormatException
     */
    static public String setMeterFeet(String input, String nameAttr) throws IllegalArgumentException, NumberFormatException {
        String temp = input.replaceAll("^\"|\"$", "");

        String lastLetter = Character.toString(temp.charAt(temp.length() - 1));

        if (!lastLetter.equals("F") && !lastLetter.equals("M") && !isNumeric(lastLetter)) {
            throw new IllegalArgumentException(nameAttr + ": may be suffixed by 'M' or 'F' to designate meters or feet.");
        }

        String decimalPart = temp.substring(0, temp.length() - 1);
        Double.parseDouble(decimalPart);
        return temp;
    }

    /**
     *
     * @param str
     * @return
     */
    static public boolean isNumeric(String str) {
        try {
            double d = Double.parseDouble(str);
        } catch (NumberFormatException nfe) {
            return false;
        }
        return true;
    }

    /**
     *
     * @param input
     * @param nameAttr
     * @return
     * @throws IllegalArgumentException
     * @throws NumberFormatException
     */
    static public String setNauticalMeterFeet(String input, String nameAttr) throws IllegalArgumentException, NumberFormatException {
        String temp = input.replaceAll("^\"|\"$", "");

        String lastLetter = Character.toString(temp.charAt(temp.length() - 1));

        if (!lastLetter.equals("F") && !lastLetter.equals("M") && !lastLetter.equals("N") && !isNumeric(lastLetter)) {
            throw new IllegalArgumentException(nameAttr + ": F, M, N suffix.");
        }

        String decimalPart = temp.substring(0, temp.length() - 1);
        Double.parseDouble(decimalPart);
        return temp;
    }

    /**
     *
     * @param input
     * @param limit
     * @return
     */
    static public String setDegMinSec(String input, double limit) {
        String temp = input.replaceAll("^\"|\"$", "");

        int degrees;
        double minSec;

        String direction = Character.toString(temp.charAt(0));

        if (!direction.equals("N") && !direction.equals("S") && !direction.equals("W") && !direction.equals("E")) {
            return null;
        }

        String temp2 = temp.substring(1, temp.length() - 1);
        Scanner s = new Scanner(temp2);

        if (!s.hasNextInt()) {
            return null;
        }
        degrees = s.nextInt();

        if (degrees < 0 || degrees > limit) {
            return null;
        }

        s.useLocale(Locale.US);
        if (!s.hasNextDouble()) {
            return null;
        }

        minSec = s.nextDouble();

        if (s.hasNext()) {
            return null;
        }

        return temp;
    }

}
