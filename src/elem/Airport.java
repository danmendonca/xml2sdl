package elem;

import java.util.ArrayList;
import java.util.HashMap;
import org.jgrapht.graph.DefaultDirectedGraph;

public class Airport {

    /**
     *
     */
    private DefaultDirectedGraph dg;
    private double lat;
    private double lon;
    private double magvar;
    private double trafficScalar;

    private String region;
    private String country;
    private String state;
    private String city;
    private String name;
    private String alt;
    private String ident;
    private String airportTestRadius;

    /**
     *
     */
    private HashMap<Integer, TaxiwayPoint> twPointMap;
    private Services servicesElem;
    private Tower towerElem;
    private HashMap<Integer, TaxiwayParking> twParkingMap;
    private ArrayList<TaxiwayPath> twPathList;
    private ArrayList<Runway> runwayList;
    private ArrayList<Helipad> helipadList;
    private ArrayList<Com> comList;
    private HashMap<Integer, TaxiName> taxiNameMap;
    

    /**
     *
     */
    public Airport() {
        lat = Double.NaN;
        lon = Double.NaN;
        magvar = Double.NaN;
        trafficScalar = Double.NaN;

        twPointMap = new HashMap<>();
    }

    /**
     *
     * @param twPointMap
     */
    public void setTwPointMap(HashMap<Integer, TaxiwayPoint> twPointMap) {
        this.twPointMap = twPointMap;
    }

    /**
     *
     * @param servicesElem
     */
    public void setServicesElem(Services servicesElem) {
        this.servicesElem = servicesElem;
    }

    /**
     *
     * @param towerElem
     */
    public void setTowerElem(Tower towerElem) {
        this.towerElem = towerElem;
    }

    /**
     *
     * @param twParkingMap
     */
    public void setTwParkingMap(HashMap<Integer, TaxiwayParking> twParkingMap) {
        this.twParkingMap = twParkingMap;
    }

    /**
     *
     * @param twPathList
     */
    public void setTwPathList(ArrayList<TaxiwayPath> twPathList) {
        this.twPathList = twPathList;
    }

    /**
     *
     * @param runwayList
     */
    public void setRunwayList(ArrayList<Runway> runwayList) {
        this.runwayList = runwayList;
    }

    /**
     *
     * @param helipadList
     */
    public void setHelipadList(ArrayList<Helipad> helipadList) {
        this.helipadList = helipadList;
    }

    /**
     *
     * @param comList
     */
    public void setComList(ArrayList<Com> comList) {
        this.comList = comList;
    }

    /**
     *
     * @param taxiNameMap
     */
    public void setTaxiNameMap(HashMap<Integer, TaxiName> taxiNameMap) {
        this.taxiNameMap = taxiNameMap;
    }

    /**
     *
     */
    public void print() {
        System.out.println("### Airport - Attributes ###");
        Utility.printString("Region", region);
        Utility.printString("Country", country);
        Utility.printString("State", state);
        Utility.printString("City", city);
        Utility.printString("Name", name);
        Utility.printDouble("Lat", lat);
        Utility.printDouble("Lon", lon);
        Utility.printString("Alt", alt);
        Utility.printDouble("Magvr", magvar);
        Utility.printString("Ident", ident);
        Utility.printString("AirportTestRadios", airportTestRadius);
        Utility.printDouble("TrafficScalar", trafficScalar);
        System.out.print(System.lineSeparator());

        servicesElem.print();
        System.out.print(System.lineSeparator());

        towerElem.print();
        System.out.print(System.lineSeparator());

        for (Runway rw : runwayList) {
            rw.print();
        }
        System.out.print(System.lineSeparator());

        for (Helipad hp : helipadList) {
            hp.print();
        }
        System.out.print(System.lineSeparator());

        for (Com comElem : comList) {
            comElem.print();
        }
        System.out.print(System.lineSeparator());

        for (TaxiwayPoint twpoint : twPointMap.values()) {
            twpoint.print();
        }
        System.out.print(System.lineSeparator());

        for (TaxiwayParking twparking : twParkingMap.values()) {
            twparking.print();
        }
        System.out.print(System.lineSeparator());

        for (TaxiName txname : taxiNameMap.values()) {
            txname.print();
        }
        System.out.print(System.lineSeparator());

        for (TaxiwayPath twpath : twPathList) {
            twpath.print();
        }
        System.out.print(System.lineSeparator());
    }

    /**
     *
     * @throws IllegalArgumentException
     */
    public void checkMissingAttr() throws IllegalArgumentException {
        Utility.checkMandatoryDouble("Lat", lat);
        Utility.checkMandatoryDouble("Lon", lon);
        Utility.checkMandatoryString("Alt", alt);
        Utility.checkMandatoryString("Ident", ident);
        Utility.checkMandatoryString("AirportTestRadius", airportTestRadius);
        Utility.checkMandatoryDouble("TrafficScalar", trafficScalar);
    }

    /**
     *
     * @param input
     */
    public void setRegion(String input) throws IllegalArgumentException {

        if (Utility.checkDuplicString(region, "Region")) {
            region = Utility.setString(input, "Region", 48);
        }
    }

    /**
     *
     * @param input
     */
    public void setCountry(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(country, "Country")) {
            country = Utility.setString(input, "Country", 48);
        }
    }

    /**
     *
     * @param input
     */
    public void setState(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(state, "State")) {
            state = Utility.setString(input, "State", 48);
        }
    }

    /**
     *
     * @param input
     */
    public void setCity(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(city, "City")) {
            city = Utility.setString(input, "City", 48);
        }
    }

    /**
     *
     * @param input
     */
    public void setName(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(name, "Name")) {
            name = Utility.setString(input, "Name", 48);
        }
    }

    /**
     *
     * @param input
     */
    public void setLat(String input) throws NumberFormatException, IllegalArgumentException {
        if (Utility.checkDuplicDouble(lat, "Lat")) {
            lat = Utility.setDouble(input, "Lat", -90.0, 90.0);
        }
    }

    /**
     *
     * @param input
     */
    public void setLon(String input) throws NumberFormatException, IllegalArgumentException {
        if (Utility.checkDuplicDouble(lon, "Lon")) {
            lon = Utility.setDouble(input, "Lon", -180.0, 180.0);
        }
    }

    /**
     *
     * @param input
     */
    public void setAlt(String input) throws IllegalArgumentException, NumberFormatException {
        if (Utility.checkDuplicString(alt, "Alt")) {
            alt = Utility.setMeterFeet(input, "Alt");
        }
    }

    /**
     *
     * @param input
     */
    public void setMagvar(String input) throws NumberFormatException, IllegalArgumentException {
        if (Utility.checkDuplicDouble(magvar, "Magvar")) {
            magvar = Utility.setDouble(input, "Magvar", -360.0, 360.0);
        }
    }

    /**
     *
     * @param input
     */
    public void setIdent(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(ident, "Ident")) {
            ident = Utility.setString(input, "Ident", 4);
        }
    }

    /**
     *
     * @param input
     */
    public void setAirportTestRadius(String input) throws IllegalArgumentException, NumberFormatException {
        if (Utility.checkDuplicString(airportTestRadius, "AirportTestRadius")) {
            airportTestRadius = Utility.setNauticalMeterFeet(input, "AirportTestRadius");
        }
    }

    /**
     *
     * @param input
     */
    public void setTrafficScalar(String input) throws NumberFormatException, IllegalArgumentException {
        if (Utility.checkDuplicDouble(trafficScalar, "TrafficScalar")) {
            trafficScalar = Utility.setDouble(input, "TrafficScalar", 0.01, 1.0);
        }
    }

    /**
     *
     * @param index
     * @return
     */
    public TaxiwayPoint getTWPoint(Integer index) {
        return twPointMap.get(index);
    }

    /**
     *
     * @param index
     * @return
     */
    public TaxiwayParking getTWParking(Integer index) {
        return twParkingMap.get(index);
    }

    /**
     *
     * @return
     */
    public ArrayList<TaxiwayPath> getTwPathList() {
        return twPathList;
    }

    /**
     * 
     * @param index
     * @return 
     */
    public TaxiName getTaxiName(Integer index) {
        return taxiNameMap.get(index);
    }

}
