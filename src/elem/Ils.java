package elem;

/**
 * Created by zikoc_000 on 12/05/2015.
 */
public class Ils {

    private double lat;
    private double lon;
    private double heading;
    private double frequency;
    private double magvar;
    private double width;

    private String alt;
    private String range;
    private String end;
    private String ident;
    private String name;
    private String backCourse;

    private GlideSlope glideSlopeElem;
    private Dme dmeElem;
    private VisualModel visualModelElem;

    public Ils() {
        lat = Double.NaN;
        lon = Double.NaN;
        heading = Double.NaN;
        frequency = Double.NaN;
        width = Double.NaN;
        magvar = Double.NaN;

        glideSlopeElem = new GlideSlope();
        dmeElem = new Dme();
        visualModelElem = new VisualModel();
    }

    /**
     *
     */
    public void print() {

        System.out.println("### ILS - Attributes ###");

        if (!Double.isNaN(lat)) {
            System.out.println("Lat = " + this.lat);
        }

        if (!Double.isNaN(lon)) {
            System.out.println("Lon = " + this.lon);
        }

        if ((this.alt != null) && !this.alt.isEmpty()) {
            System.out.println("Alt = " + this.alt);
        }
        if (!Double.isNaN(heading)) {
            System.out.println("Heading = " + this.heading);
        }
        if (!Double.isNaN(frequency)) {
            System.out.println("Frequency = " + this.frequency);
        }
        if ((this.end != null) && !this.end.isEmpty()) {
            System.out.println("End= " + this.end);
        }

        if ((this.range != null) && !this.range.isEmpty()) {
            System.out.println("Range= " + this.range);
        }
        if (!Double.isNaN(magvar)) {
            System.out.println("Magvar = " + this.magvar);
        }
        if ((this.ident != null) && !this.ident.isEmpty()) {
            System.out.println("Ident= " + this.ident);
        }
        if (!Double.isNaN(width)) {
            System.out.println("Width = " + this.width);
        }
        if ((this.name != null) && !this.name.isEmpty()) {
            System.out.println("Name= " + this.name);
        }

        if ((this.backCourse != null) && !this.backCourse.isEmpty()) {
            System.out.println("BackCourse= " + this.backCourse);
        }

        System.out.print(System.lineSeparator());

        glideSlopeElem.print();
        dmeElem.print();
        visualModelElem.print();
    }

    public void setGlideSlope(GlideSlope g) {
        glideSlopeElem = g;
    }

    public void setDme(Dme d) {
        dmeElem = d;
    }

    public void setVisualModel(VisualModel v) {
        visualModelElem = v;
    }

    /**
     *
     * @throws IllegalArgumentException
     */
    public void checkMissingAttr() throws IllegalArgumentException {
        Utility.checkMandatoryDouble("Lat", lat);
        Utility.checkMandatoryDouble("Lon", lon);
        Utility.checkMandatoryString("Alt", alt);
        Utility.checkMandatoryDouble("Heading", heading);
        Utility.checkMandatoryDouble("Frequency", frequency);
        Utility.checkMandatoryString("End", end);
        Utility.checkMandatoryDouble("Magvar", magvar);
        Utility.checkMandatoryString("Ident", ident);
    }

    /**
     *
     * @param input
     */
    public void setLat(String input) throws NumberFormatException, IllegalArgumentException {
        if (Utility.checkDuplicDouble(lat, "Lat")) {
            String latStr = input.replaceAll("^\"|\"$", "");
            this.lat = Double.parseDouble(latStr);

            if ((this.lat < -90) || (this.lat > 90)) {
                this.lat = Double.NaN;
                throw new IllegalArgumentException("Lat: -90 to +90 degrees");
            }
        }
    }

    /**
     *
     * @param input
     */
    public void setLon(String input) throws NumberFormatException, IllegalArgumentException {
        if (Utility.checkDuplicDouble(lon, "Lon")) {
            String lonStr = input.replaceAll("^\"|\"$", "");
            this.lon = Double.parseDouble(lonStr);

            if ((this.lon < -180) || (this.lon > 180)) {
                this.lon = Double.NaN;
                throw new IllegalArgumentException("Lon: -180 to +180 degrees");
            }
        }
    }

    /**
     *
     * @param input
     */
    public void setAlt(String input) {
        if (Utility.checkDuplicString(alt, "Alt")) {
            this.alt = Utility.setMeterFeet(input, "Alt");
        }
    }

    /**
     *
     * @param input
     * @throws NumberFormatException
     * @throws IllegalArgumentException
     */
    public void setHeading(String input) throws NumberFormatException, IllegalArgumentException {
        if (Utility.checkDuplicDouble(heading, "Heading")) {
            String headingStr = input.replaceAll("^\"|\"$", "");
            heading = Double.parseDouble(headingStr);

            if ((heading < 0.0) || (heading > 360.0)) {
                heading = Double.NaN;
                throw new IllegalArgumentException("Heading: 0.0 to 360.0");
            }
        }
    }

    /**
     *
     * @param input
     * @throws NumberFormatException
     * @throws IllegalArgumentException
     */
    public void setFrequency(String input) throws NumberFormatException, IllegalArgumentException {
        if (Utility.checkDuplicDouble(frequency, "Frequency")) {
            String frequencyStr = input.replaceAll("^\"|\"$", "");
            frequency = Double.parseDouble(frequencyStr);

            if ((frequency < 108.0) || (frequency > 136.992)) {
                frequency = Double.NaN;
                throw new IllegalArgumentException("Frequency: 108.0 to 136.992");
            }
        }
    }

    /**
     *
     * @param input
     * @throws IllegalArgumentException
     */
    public void setEnd(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(end, "End")) {
            String endStr = input.replaceAll("^\"|\"$", "");

            switch (endStr) {
                case "PRIMARY":
                    this.end = "PRIMARY";
                    break;
                case "SECONDARY":
                    this.end = "SECONDARY";
                    break;
                default:
                    throw new IllegalArgumentException("End: Invalid end");
            }
        }
    }

    /**
     *
     * @param input
     */
    public void setRange(String input) {
        if (Utility.checkDuplicString(range, "Range")) {
            range = Utility.setNauticalMeterFeet(input, "Range");
        }
    }

    /**
     *
     * @param input
     * @throws NumberFormatException
     * @throws IllegalArgumentException
     */
    public void setMagvar(String input) throws NumberFormatException, IllegalArgumentException {
        if (Utility.checkDuplicDouble(magvar, "Magvar")) {
            magvar = Utility.setDouble(input, "Magvar", -360.0, 360.0);
        }
    }

    /**
     *
     * @param input
     * @throws IllegalArgumentException
     */
    public void setIdent(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(ident, "Ident")) {
            this.ident = input.replaceAll("^\"|\"$", "");

            if (this.ident.length() > 5) {
                this.ident = null;
                throw new IllegalArgumentException("Ident: String (5 characters max)");
            }
        }
    }

    /**
     *
     * @param input
     * @throws NumberFormatException
     * @throws IllegalArgumentException
     */
    public void setWidth(String input) throws NumberFormatException, IllegalArgumentException {
        if (Utility.checkDuplicDouble(width, "Width")) {
            String widthStr = input.replaceAll("^\"|\"$", "");
            width = Double.parseDouble(widthStr);

            if ((width < 0.0) || (width > 360.0)) {
                width = Double.NaN;
                throw new IllegalArgumentException("Width: 0.0 to 360.0");
            }
        }
    }

    /**
     *
     * @param input
     * @throws IllegalArgumentException
     */
    public void setName(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(name, "Name")) {
            this.name = input.replaceAll("^\"|\"$", "");

            if (this.name.length() > 48) {
                this.name = null;
                throw new IllegalArgumentException("Name: String (48 characters max)");
            }
        }
    }

    /**
     *
     * @param input
     * @throws IllegalArgumentException
     */
    public void setBackCourse(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(backCourse, "Backcourse")) {
            String backcourseStr = input.replaceAll("^\"|\"$", "");

            switch (backcourseStr) {
                case "TRUE":
                    this.backCourse = "TRUE";
                    break;
                case "FALSE":
                    this.backCourse = "FALSE";
                default:
                    throw new IllegalArgumentException("BACKCOURSE: Invalid backcourse");
            }
        }
    }
}
