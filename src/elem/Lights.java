package elem;

import java.util.HashSet;

public class Lights {

    private String center;
    private String edge;
    private String centerRed;

    private boolean centerSet;
    private boolean edgeSet;
    private boolean centerRedSet;

    private final static HashSet<String> centerEdgeSet;

    static {
        centerEdgeSet = new HashSet<>();
        centerEdgeSet.add(""); // User can enter the empty string
        centerEdgeSet.add("NONE");
        centerEdgeSet.add("LOW");
        centerEdgeSet.add("MEDIUM");
        centerEdgeSet.add("HIGH");
    }

    public Lights() {
        centerSet = false;
        edgeSet = false;
        centerRedSet = false;
    }

    /**
     *
     * @throws IllegalArgumentException
     */
    public void checkMissingAttr() throws IllegalArgumentException {
        Utility.checkMandatoryString("Center", center);
        Utility.checkMandatoryString("Edge", edge);
        Utility.checkMandatoryString("CenterRed", centerRed);
    }

    /**
     * 
     */
    public void print() {
        System.out.println("### Lights - Attributes ###");

        if (centerSet) {
            System.out.println("Center = " + center);
        }

        if (edgeSet) {
            System.out.println("Edge = " + edge);
        }

        if (centerRedSet) {
            System.out.println("Center Red = " + centerRed);
        }

        System.out.print(System.lineSeparator());
    }

    /**
     * 
     * @param input
     * @throws IllegalArgumentException 
     */
    public void setCenter(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(center, "Center")) {
            String inputStr = input.replaceAll("^\"|\"$", "");

            if (centerEdgeSet.contains(inputStr)) {
                centerSet = true;
                center = inputStr;

                if (center.isEmpty()) {
                    center = "NONE";
                }
            } else {
                throw new IllegalArgumentException("Input: Invalid CENTER value. Valid values are: NONE, LOW, MEDIUM, HIGH.");
            }
        }
    }

    /**
     * 
     * @param input
     * @throws IllegalArgumentException 
     */
    public void setEdge(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(edge, "Edge")) {
            String inputStr = input.replaceAll("^\"|\"$", "");

            if (centerEdgeSet.contains(inputStr)) {
                edgeSet = true;
                edge = inputStr;

                if (edge.isEmpty()) {
                    edge = "NONE";
                }
            } else {
                throw new IllegalArgumentException("Input: Invalid EDGE value. Valid values are: NONE, LOW, MEDIUM, HIGH.");
            }
        }
    }

    /**
     * 
     * @param input
     * @throws IllegalArgumentException 
     */
    public void setCenterRed(String input) throws IllegalArgumentException {
        if (Utility.checkDuplicString(centerRed, "CenterRed")) {
            String inputStr = input.replaceAll("^\"|\"$", "");
            if (inputStr.equals("TRUE") || inputStr.equals("FALSE")) {
                centerRedSet = true;
                centerRed = inputStr;
            } else {
                throw new IllegalArgumentException("Center Red: Valid values are TRUE or FALSE.");
            }
        }
    }
}
