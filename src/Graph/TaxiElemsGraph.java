/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Graph;

import elem.TaxiElemsWrapper;
import elem.TaxiwayParking;
import elem.TaxiwayPoint;
import java.util.HashMap;
import Graph.RelationshipEdge;
import elem.TaxiwayPath;
import java.util.ArrayList;
import org.jgrapht.graph.DefaultDirectedGraph;

/**
 *
 * @author Daniel
 */
public class TaxiElemsGraph {

    //airport elements
    ArrayList<TaxiwayPath> twPaths;
    HashMap<Integer, TaxiwayParking> twParkings;
    HashMap<Integer, TaxiwayPoint> twPoints;

    
    
    DefaultDirectedGraph<TaxiElemsWrapper, RelationshipEdge> dg;


    /**
     * 
     * @param twPaths
     * @param twParkings
     * @param twPoints 
     */
    public TaxiElemsGraph(ArrayList<TaxiwayPath> twPaths,
            HashMap<Integer, TaxiwayParking> twParkings,
            HashMap<Integer, TaxiwayPoint> twPoints
    ){    
        
        dg = new DefaultDirectedGraph<>(RelationshipEdge.class);
        this.twPaths = twPaths;
        this.twParkings = twParkings;
        this.twPoints = twPoints;
    }

    /**
     * @return
     */
    public boolean setEdges() {

        for (TaxiwayPath tPath : twPaths) {

            switch (tPath.getType().toUpperCase()) {
                case "RUNWAY":
                    break;
                case "TAXI":
                    break;
                case "PARKING":
                    break;
                default:
                    ;
            }
        }

        return true;
    }

    private boolean setRunwayEdge(TaxiwayPath tpath) {

        int starttTp = tpath.getStart();
        int endtTp = tpath.getEnd();
        
        TaxiwayPoint startTwP = twPoints.get(starttTp);

        return false;
    }

}
