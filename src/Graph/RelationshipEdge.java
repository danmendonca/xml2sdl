/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Graph;

import org.jgrapht.graph.DefaultEdge;

/**
 *
 * @author Daniel
 */
public class RelationshipEdge<T> extends DefaultEdge {

    private T t1;
    private T t2;
    private String label;
    
    public RelationshipEdge(T t1, T t2){
        
    }

    public T getT1() {
        return t1;
    }

    public void setV1(T v1) {
        this.t1 = v1;
    }

    public T getT2() {
        return t2;
    }

    public void setT2(T t2) {
        this.t2 = t2;
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
    
    

}
