grammar ROUTE;

@members {          
    boolean tagMode = false;
}

//=====================================
//LEXER
//=====================================
TAG_START_OPEN : '<' {tagMode = true;};
TAG_END_OPEN : '</' {tagMode = true;};
TAG_CLOSE : {tagMode}? '>' {tagMode = false;};
TAG_EMPTY_CLOSE : {tagMode}? '/>' {tagMode = false;};

//Attribute values
ATTR_EQ : {tagMode}? '=';
ATTR_DQ : {tagMode}? '"';
ATTR_QU : {tagMode}? '\'';

WS: [ \t\r\n]+ -> skip ;

//INT
ATTR_VALUE_INT : { tagMode }? ( ATTR_DQ ('-')?(DIGIT)* ATTR_DQ | ATTR_QU ('-')?(DIGIT)* ATTR_QU);

//REAL
ATTR_VALUE_REAL : { tagMode }? (  ('-')?(DIGIT)*('.'DIGIT)+  | ATTR_QU ('-')?(DIGIT)*'.'(DIGIT)+ ATTR_QU) 
| (  ('-')?(DIGIT)+'.'(DIGIT)* | ATTR_QU ('-')?(DIGIT)+'.'(DIGIT)* ATTR_QU) ;

//STR
ATTR_VALUE_STR : { tagMode }? ( ATTR_DQ (NAMECHAR)* ATTR_DQ | ATTR_QU (NAMECHAR)* ATTR_QU);

//~ -> this means anything but the character that follows
PCDATA : {!tagMode}? (~'<')+ ;

NAMECHAR : LETTER | DIGIT | '.' | '-' | '_' | ':' ;
DIGIT : [0-9] ;
LETTER : [a-z] | [A-Z] ;


//COMMON ATTRIBUTES
LAT : 'lat';
LON : 'lon';
ALT : 'alt';
TYPE : 'type';

//=====================================
//PARSER
//=====================================
//XML Doc
document : xmlElem EOF;

//XML Element
xmlElem : routeStart
         (previouselement nextelement)?
          routeEnd;



//=====================================
//ROUTE
//=====================================
ROUTE_TAG: 'Route';

//Route - attributes
ROUTE_ROUTETYPE : 'routeType';
ROUTE_NAME : 'name';


//Route - Rules
routeStart:  TAG_START_OPEN ROUTE_TAG
             (routeAttrStr ATTR_EQ ATTR_VALUE_STR)*
             TAG_CLOSE; 

routeEnd : TAG_END_OPEN ROUTE_TAG TAG_CLOSE;


routeAttrStr : ROUTE_ROUTETYPE
             | ROUTE_NAME;
              
//=====================================
//PREVIOUS
//=====================================
PREVIOUS_TAG : 'Previous';

//PreviousandNEXT common- Attributes
WAYPOINTTYPE : 'waypointType';
WAYPOINTREGION : 'waypointRegion';
WAYPOINTIDENT : 'waypointIdent';
ALTITUDEMINIMUN : 'altitudeMinimum';


//Previous - Rules
previouselement : TAG_START_OPEN PREVIOUS_TAG
                  ((previousAttrStr ATTR_EQ ATTR_VALUE_STR) | (previousAttrReal ATTR_EQ ( ATTR_VALUE_REAL | ATTR_VALUE_INT )))*
                  TAG_EMPTY_CLOSE;

previousAttrStr : WAYPOINTTYPE
                | WAYPOINTREGION
                | WAYPOINTIDENT;
                   
previousAttrReal : ALTITUDEMINIMUN;


//=====================================
//NEXT
//=====================================
NEXT_TAG : 'Next';


//Next - Rules
nextelement : TAG_START_OPEN NEXT_TAG
                  ((nextAttrStr ATTR_EQ ATTR_VALUE_STR) | (nextAttrReal  ATTR_EQ (ATTR_VALUE_REAL | ATTR_VALUE_INT )))*
                  TAG_EMPTY_CLOSE;

nextAttrStr : WAYPOINTTYPE
               | WAYPOINTREGION
               | WAYPOINTIDENT;
                   
nextAttrReal : ALTITUDEMINIMUN;


