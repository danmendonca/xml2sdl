grammar TaxiwayPath;


@members {
          boolean tagMode = false;
}


//=====================================
//LEXER
//=====================================
TAG_START_OPEN: '<' {tagMode = true;};
TAG_EMPTY_CLOSE: {tagMode}? '/>' {tagMode = false;};

//Attribute values
ATTR_EQ : {tagMode}? '=';
ATTR_DQ : {tagMode}? '"';
ATTR_QU : {tagMode}? '\'';
ATTR_VALUE : { tagMode }? ( ATTR_DQ (~'"')* ATTR_DQ | ATTR_QU (~'\'')* ATTR_QU);


WS: [ \t\r\n]+ -> skip ;


//~ -> this means anything but the character that follows
PCDATA : {!tagMode}? (~'<')+ ;

NAMECHAR : LETTER | DIGIT | '.' | '-' | '_' | ':' ;
DIGIT : [0-9] ;
LETTER : [a-z] | [A-Z] ;


//=====================================
//PARSER
//=====================================
document: startTag EOF;

//<p>
startTag: TAG_START_OPEN taxiwayPathElem TAG_EMPTY_CLOSE;
          

//Attribute value rules
//INT
attributeValueInt : ATTR_VALUE {
                                try{
                                    String str = new String($ATTR_VALUE.text);
                                    int result = Integer.parseInt(str.replaceAll("^\"|^\'|\'$|\"$", ""));
                                } catch(NumberFormatException e){
                                    System.err.println($ATTR_VALUE.line + ":" + $ATTR_VALUE.pos + " - " + "Error: Attribute value should be a integer.");
                                }
                                
};

//REAL
attributeValueReal : ATTR_VALUE {
                                 try{
                                    String str = new String($ATTR_VALUE.text);
                                    double result = Double.parseDouble(str.replaceAll("^\"|^\'|\'$|\"$", ""));
                                } catch(NumberFormatException e){
                                    System.err.println($ATTR_VALUE.line + ":" + $ATTR_VALUE.pos + " - " + "Error: Attribute value should be a double.");
                                }
};

//STRING
attributeValueString : ATTR_VALUE;



//=====================================
//TaxiwayPath
//=====================================
TAXIWAYPATH_TAG: 'TaxiwayPath';


//TaxiwayPath attributes(not optional): type, start, end, width, weightLimit, surface,
//drawSurface, drawDetail, number, designator, name
TYPE: 'type';
START: 'start';
END: 'end';
WIDTH: 'width';
WEIGHT_LIMIT: 'weightLimit';
SURFACE: 'surface';
DRAW_SURFACE: 'drawSurface';
DRAW_DETAIL: 'drawDetail';
NUMBER: 'number';
DESIGNATOR: 'designator';
NAME: 'name';


//TaxiwayPath attributes(optional): centerLine, centerLineLighted, leftEdge, leftEdgeLighted,
//rightEdge, rightEdgeLighted
CENTER_LINE_OP: 'centerLine';
CENTER_LINE_LIGHTED_OP: 'centerLineLighted';
LEFT_EDGE_OP: 'leftEdge';
LEFT_EDGE_LIGHTED_OP: 'leftEdgeLighted';
RIGHT_EDGE_OP: 'rightEdge';
RIGHT_EDGE_LIGHTED_OP: 'rightEdgeLighted';


//TaxiwayPath RULES
taxiwayPathElem: TAXIWAYPATH_TAG
                 ((taxiwayPathAttrInt ATTR_EQ attributeValueInt) |
                 (taxiwayPathAttrReal ATTR_EQ attributeValueReal) |
                 (taxiwayPathAttrString ATTR_EQ attributeValueString))*;
                   

taxiwayPathAttrInt: START | 
                    END |
                    NAME;


taxiwayPathAttrReal: WIDTH |
                     WEIGHT_LIMIT ;


taxiwayPathAttrString: TYPE |
                       SURFACE |
                       DRAW_SURFACE |
                       DRAW_DETAIL |
                       CENTER_LINE_OP |
                       CENTER_LINE_LIGHTED_OP |
                       LEFT_EDGE_OP |
                       LEFT_EDGE_LIGHTED_OP |
                       RIGHT_EDGE_OP |
                       RIGHT_EDGE_LIGHTED_OP |
                       NUMBER |
                       DESIGNATOR;