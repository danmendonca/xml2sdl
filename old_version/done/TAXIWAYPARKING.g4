grammar TAXIWAYPARKING;

@members {          
    boolean tagMode = false; 
    boolean airportMode = false;
    boolean taxiwayparking = false;
}

//=====================================
//LEXER
//=====================================
TAG_START_OPEN : '<' {tagMode = true;};
TAG_END_OPEN : '</' {tagMode = true;};
TAG_CLOSE : {tagMode}? '>' {tagMode = false;};
TAG_EMPTY_CLOSE : {tagMode}? '/>' {tagMode = false;};

//Attribute values
ATTR_EQ : {tagMode}? '=';
ATTR_DQ : {tagMode}? '"';
ATTR_QU : {tagMode}? '\'';
ATTR_VALUE : { tagMode }? ( ATTR_DQ (~'"')* ATTR_DQ | ATTR_QU (~'\'')* ATTR_QU);

//~ -> this means anything but the character that follows
PCDATA : {!tagMode}? (~'<')+ ;

NAMECHAR : LETTER | DIGIT | '.' | '-' | '_' | ':' ;
DIGIT : [0-9] ;
LETTER : [a-z] | [A-Z] ;
WS: [ \t\r\n]+ -> skip ;

//COMMON ATTRIBUTES
//LAT : 'lat';
//LON : 'lon';
//ALT : 'alt';
//YPE : 'type';

//=====================================
//PARSER
//=====================================
//XML Doc
document : element EOF;

//XML Element
element : startTag ( element | PCDATA )* endTag
          | emptyElement;

//<p>
startTag : TAG_START_OPEN taxiwayparkingElemExp TAG_CLOSE;

//<img/>
emptyElement : TAG_START_OPEN taxiwayparkingElemExp TAG_EMPTY_CLOSE ;

//</p>
endTag : TAG_END_OPEN taxiwayparkingElemTag TAG_CLOSE;

//All Taxiwayparking elements that have startTag
taxiwayparkingElemExp : taxiwayparkingElem ;

//All Taxiwayparking elements tags that have endTag
taxiwayparkingElemTag : TAXIWAYPARKING_TAG ;


//Attribute value rules
//INT
attributeValueInt : ATTR_VALUE {
                                try{
                                    String str = new String($ATTR_VALUE.text);
                                    int result = Integer.parseInt(str.replaceAll("^\"|^\'|\'$|\"$", ""));
                                } catch(NumberFormatException e){
                                    System.err.println($ATTR_VALUE.line + ":" + $ATTR_VALUE.pos + " - " + "Error: Attribute value should be a integer.");
                                }
                                
};

//REAL
attributeValueReal : ATTR_VALUE {
                                 try{
                                    String str = new String($ATTR_VALUE.text);
                                    double result = Double.parseDouble(str.replaceAll("^\"|^\'|\'$|\"$", ""));
                                } catch(NumberFormatException e){
                                    System.err.println($ATTR_VALUE.line + ":" + $ATTR_VALUE.pos + " - " + "Error: Attribute value should be a double.");
                                }
};

//STRING
attributeValueStr : ATTR_VALUE;

//=====================================
//TaxiwayParking
//=====================================

TAXIWAYPARKING_TAG : 'TaxiwayParking' ;

//(taxiwayparking - Attributes): index,lat,lon,biasX,biasZ,heading,radius,type,name,number,
//pushBack,

TAXIWAYPARKING_INDEX : 'index' ;
TAXIWAYPARKING_LAT : 'lat';
TAXIWAYPARKING_LON : 'lon';
TAXIWAYPARKING_BIASX : 'biasX';
TAXIWAYPARKING_BIASZ : 'biasZ';
TAXIWAYPARKING_HEADING : 'heading';
TAXIWAYPARKING_RADIUS : 'radius';
TAXIWAYPARKING_TYPE : 'type';
TAXIWAYPARKING_NAME : 'name';
TAXIWAYPARKING_NUMBER : 'number';

//(taxiwayparking - Attributes(optional)): airlineCodes,pushBack,teeOffset1,teeOffset2,teeOffset3,
//teeOffset4
TAXIWAYPARKING_ACODES_OP : 'airlineCodes';
TAXIWAYPARKING_PBACK_OP : 'pushBack';
TAXIWAYPARKING_TOSET1_OP : 'teeOffset1';
TAXIWAYPARKING_TOSET2_OP : 'teeOffset2';
TAXIWAYPARKING_TOSET3_OP : 'teeOffset3';
TAXIWAYPARKING_TOSET4_OP : 'teeOffset4';


//Taxiwayparking - Rules
taxiwayparkingElem : TAXIWAYPARKING_TAG
                      ((taxiwayparkingAttrInt ATTR_EQ attributeValueInt)|(taxiwayparkingAttrReal ATTR_EQ attributeValueReal) | (taxiwayparkingAttrStr ATTR_EQ attributeValueStr))*; 



taxiwayparkingAttrInt: TAXIWAYPARKING_INDEX
                     | TAXIWAYPARKING_NUMBER;
                         
                         
                         
taxiwayparkingAttrReal: TAXIWAYPARKING_LAT
                      | TAXIWAYPARKING_LON
                      | TAXIWAYPARKING_BIASX
                      | TAXIWAYPARKING_BIASZ
                      | TAXIWAYPARKING_HEADING
                      | TAXIWAYPARKING_RADIUS
                      | TAXIWAYPARKING_TOSET1_OP
                      | TAXIWAYPARKING_TOSET2_OP
                      | TAXIWAYPARKING_TOSET3_OP
                      | TAXIWAYPARKING_TOSET4_OP;
                          
taxiwayparkingAttrStr: TAXIWAYPARKING_TYPE
                     | TAXIWAYPARKING_NAME
                     | TAXIWAYPARKING_ACODES_OP
                     | TAXIWAYPARKING_PBACK_OP;


