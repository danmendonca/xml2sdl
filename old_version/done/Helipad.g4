/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

grammar Helipad;

@members {          
    boolean airportMode = false;
}

//=====================================
//Helipad
//=====================================
//TAG
HELIPAD : 'Helipad';

//ATTRIBUTES
CLOSED: 'closed';
TRANSPARENT : 'transparent';
RED : 'red';
GREEN : 'green';
BLUE: 'blue';


//RULES
helipadAttStr: LAT
             | LON
             | ALT
             | SURFACE
             | LENGTH
             | WIDTH
             | TYPE
             | CLOSED
             | TRANSPARENT
             ;
helipadAttReal: HEADING
              | LENGTH
              | WIDTH
              ;
helipadAttInt: RED
             | GREEN
             | BLUE
             | HEADING
              ;


helipadElem: TAG_START_OPEN HELIPAD
             ( helipadAttInt ATTR_EQ ATTR_VALUE_INT
             | helipadAttReal ATTR_EQ ATTR_VALUE_REAL
             | helipadAttStr ATTR_EQ ATTR_VALUE_STR ) *
             TAG_EMPTY_CLOSE
           ;


//============================== END =========================================

TAG_START_AIRPORT: '<Airport' {airportMode = true;};
TAG_END_AIRPORT: 'Airport>' {airportMode = false;};
TAG_START_OPEN : '<';
TAG_END_OPEN : '</';
TAG_CLOSE : '>';
TAG_EMPTY_CLOSE : '/>';

//Attribute values
ATTR_EQ : '=';
ATTR_DQ : '"';
ATTR_QU : '\'';

//TAGS
AIRPORT : 'Airport';
TAXIWAYPOINT : 'TaxiwayPoint';
SERVICES : 'Services';
FUEL : 'Fuel';
TOWER : 'Tower';
TAXIWAYPARKING : 'TaxiwayParking';
ROUTE : 'Route';
PREVIOUS : 'Previous';
NEXT : 'Next';
TAXIWAYPATH: 'TaxiwayPath';
RUNWAY_ALIAS: 'RunwayAlias';
COM: 'Com';
TAXINAME : 'TaxiName';
RUNWAY: 'Runway';
MARKINGS : 'Markings';
LIGHTS: 'Lights';
OFFSETTHRESHOLD: 'OffsetThreshold';
BLASTPAD: 'BlastPad';
OVERRUN : 'Overrun';
APPROACHLIGHTS : 'ApproachLights';
VASI : 'Vasi';
ILS : 'Ils';
GLIDESLOPE : 'GlideSlope';
DME : 'Dme';
VISUALMODEL : 'VisualModel';
BiasXYZ : 'BiasXYZ';
RUNWAYSTART : 'RunwayStart';

//ATTRIBUTES
LAT : 'lat';
LON : 'lon';
ALT : 'alt';
TYPE : 'type';
INDEX : 'index';
BIASX:'biasX';
BIASZ:'biasZ';
NAME : 'name';
NUMBER: 'number';
IDENT : 'ident';
MAGVAR : 'magvar';
AIRPORT_TEST_RADIUS : 'airportTestRadius';
TRAFFIC_SCALAR : 'trafficScalar';
REGION : 'region';
COUNTRY : 'country';
STATE : 'state';
CITY : 'city';
ORIENTATION: 'orientation';
AVAILABILITY : 'availability';
HEADING : 'heading';
RADIUS : 'radius';
AIRLINECODES : 'airlineCodes';
PUSHBACK : 'pushBack';
TEEOFFSET1 : 'teeOffset1';
TEEOFFSET2 : 'teeOffset2';
TEEOFFSET3 : 'teeOffset3';
TEEOFFSET4 : 'teeOffset4';
ROUTETYPE : 'routeType';
WAYPOINTTYPE : 'waypointType';
WAYPOINTREGION : 'waypointRegion';
WAYPOINTIDENT : 'waypointIdent';
ALTITUDEMINIMUN : 'altitudeMinimum';
START : 'start';
END : 'end';
WIDTH : 'width';
WEIGHT_LIMIT : 'weightLimit';
SURFACE : 'surface';
DRAW_SURFACE : 'drawSurface';
DRAW_DETAIL : 'drawDetail';
DESIGNATOR : 'designator';
CENTER_LINE : 'centerLine';
CENTER_LINE_LIGHTED : 'centerLineLighted';
LEFT_EDGE : 'leftEdge';
LEFT_EDGE_LIGHTED : 'leftEdgeLighted';
RIGHT_EDGE : 'rightEdge';
RIGHT_EDGE_LIGHTED : 'rightEdgeLighted';
FREQUENCY: 'frequency';
LENGTH: 'length';
PRIMARY_DESIGNATOR: 'primaryDesignator';
SECONDARY_DESIGNATOR: 'secondaryDesignator';
PATTERN_ALTITUDE: 'patternAltitude';
PRIMARY_TAKE_OFF: 'primaryTakeoff';
PRIMARY_LANDING: 'primaryLanding';
PRIMARY_PATTERN: 'primaryPattern';
SECONDARY_TAKE_OFF: 'secondaryTakeoff';
SECONDARY_LANDING: 'secondaryLanding';
SECONDARY_PATTERN: 'secondaryPattern';
PRIMARY_MARKING_BIAS: 'primaryMarkingBias';
SECONDARY_MARKING_BIAS: 'secondaryMarkingBias';
ALTERNATETHRESHOLD: 'alternateThreshold';
ALTERNATETOUCHDOWN: 'alternateTouchdown';
ALTERNATEFIXEDDISTANCE: 'alternateFixedDistance';
ALTERNATEPRECISION: 'alternatePrecision';
LEADINGZEROSIDENT: 'leadingZeroIdent';
NOTHRESHOLDENDARROWS: 'noThresholdEndArrows';
EDGES: 'edges';
THRESHOLD: 'threshold';
FIXED_DISTANCE: 'fixedDistance';
FIXED:'fixed';
TOUCHDOWN: 'touchdown';
DASHES: 'dashes';
PRECISION: 'precision';
EDGEPAVEMENT:'edgePavement';
SINGLEEND:'singleEnd';
PRIMARYCLOSE:'primaryClosed';
SECONDARYCLOSED:'secondaryClosed';
PRIMARYSTOL: 'primaryStol';
SECONDARYSTOL:'secondaryStol';
CENTER:'center';
EDGE:'edge';
CENTERRED:'centerRed';
SYSTEM: 'system';
REIL : 'reil';
ENDLIGHTS: 'endLights';
STROBES: 'strobes';
SIDE:'side';
SPACING:'spacing';
PITCH:'pitch';
RANGE: 'range';
BACKCOURSE: 'backCourse';
IMAGECOMPLEXITY: 'imageComplexity';
BIASY: 'biasY';

//INT
ATTR_VALUE_INT : (ATTR_DQ ('-')?(DIGIT)+ ATTR_DQ | ATTR_QU ('-')?(DIGIT)+ ATTR_QU);

//REAL
ATTR_VALUE_REAL : ((ATTR_DQ '-'? DIGIT+ '.' DIGIT+ ATTR_DQ) | (ATTR_QU '-'? DIGIT+ '.' DIGIT+ ATTR_QU));

//STR
ATTR_VALUE_STR : (ATTR_DQ (~'"')* ATTR_DQ | ATTR_QU (~'\'')* ATTR_QU);

//~ -> this means anything but the character that follows
PCDATA : {!airportMode}? (~'<')+ ;

NAMECHAR : LETTER | DIGIT | '.' | '-' | '_' | ':';
DIGIT : [0-9] ;
LETTER : [a-z] | [A-Z] ;
WS: [ \t\r\n]+ -> skip ;

//=====================================
//PARSER
//=====================================
//XML Doc
document : PCDATA* (xmlElem PCDATA?)+ EOF;

//XML Element
xmlElem : airportStart
          //serviceElem?
          //towerElem?
          //runwayElem*
          //comElem*
          //taxiwaypointElem+
          //taxiwayparkingElem+
          //taxiNameElem+
          //taxiwayPathElem+
          //runwayAliasElem?
          //airportEnd
          helipadElem
        ;

/*
//INT
attributeValueInt : ATTR_VALUE_INT {
                                try{
                                    String str = new String($ATTR_VALUE_INT.text);
                                    int result = Integer.parseInt(str.replaceAll("^\"|^\'|\'$|\"$", ""));
                                } catch(NumberFormatException e){
                                    System.err.println($ATTR_VALUE_INT.line + ":" + $ATTR_VALUE_INT.pos + " - " + "Error: Attribute value should be a integer.");
                                }
};
*/

//=====================================
//Airport
//=====================================
airportAttrReal : LAT
                | LON
                | MAGVAR
                | TRAFFIC_SCALAR
                | AIRPORT_TEST_RADIUS;

airportAttrStr : REGION
               | COUNTRY
               | STATE
               | CITY
               | NAME
               | ALT
               | IDENT
               | AIRPORT_TEST_RADIUS;

airportStart : TAG_START_AIRPORT
             ((airportAttrReal ATTR_EQ ATTR_VALUE_REAL) | (airportAttrStr ATTR_EQ ATTR_VALUE_STR))*
             TAG_CLOSE; 

airportEnd : TAG_END_OPEN TAG_END_AIRPORT;