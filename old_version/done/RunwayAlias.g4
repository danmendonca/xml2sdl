grammar RunwayAlias;


@members {
          boolean tagMode = false;
}


//=====================================
//LEXER
//=====================================
TAG_START_OPEN: '<' {tagMode = true;};
TAG_EMPTY_CLOSE: {tagMode}? '/>' {tagMode = false;};

//Attribute values
ATTR_EQ : {tagMode}? '=';
ATTR_DQ : {tagMode}? '"';
ATTR_QU : {tagMode}? '\'';
ATTR_VALUE : { tagMode }? ( ATTR_DQ (~'"')* ATTR_DQ | ATTR_QU (~'\'')* ATTR_QU);


WS: [ \t\r\n]+ -> skip ;


//~ -> this means anything but the character that follows
PCDATA : {!tagMode}? (~'<')+ ;

NAMECHAR : LETTER | DIGIT | '.' | '-' | '_' | ':' ;
DIGIT : [0-9] ;
LETTER : [a-z] | [A-Z] ;


//=====================================
//PARSER
//=====================================
document: startTag EOF;

//<p>
startTag: TAG_START_OPEN runwayAliasElem TAG_EMPTY_CLOSE;

//STRING
attributeValueString : ATTR_VALUE;



//=====================================
//RunwayAlias
//=====================================
RUNWAY_ALIAS_TAG: 'RunwayAlias';


//RunwayAlias attributes(not optional): number, designator
DESIGNATOR: 'designator';
NUMBER: 'number';


//RunwayAlias rules
runwayAliasElem: RUNWAY_ALIAS_TAG
                 ((runwayAliasAttrString ATTR_EQ attributeValueString))*;


runwayAliasAttrString: DESIGNATOR |
                       NUMBER;